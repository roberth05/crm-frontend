import Layout from '../components/layout.js'
import React from 'react'
import { withAuthSync } from '../src/auth';
import CompaniesTable from '../components/companies-table';
import CompanyForm from '../components/company-form.js';
import { Grid } from 'semantic-ui-react';
import { inject, observer } from 'mobx-react';

@inject('uiStore', 'companyStore')
@observer
class Companies extends React.Component {
    static async getInitialProps(ctx) {
        await ctx.mobxStore.rootStore.companyStore.loadCompanies()
    }

    handleSaveCompany = async (data) => {
        try {
            console.log(`handleSaveCompany: ${JSON.stringify(data)}`);
            this.props.companyStore.saveNew(data)
            
        } catch (err) {
            throw err
        }
    };

    render() {
        const { error, isAuthenticated } = this.props;
        if (error) { console.log(`Error in render: ${error}`) }
        //if (people) { console.log(`People returned: ${JSON.stringify(people)}`) };
        if (!error) {
            return (
                <Layout title={"Companies"} isAuthenticated={isAuthenticated} loggedUser={this.props.username}>
                    <Grid>
                        <Grid.Column width={12}>
                            <CompaniesTable companies={this.props.companyStore.sortedCompanies} />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <CompanyForm onSave={this.handleSaveCompany} />
                        </Grid.Column>
                    </Grid>
                </Layout>
            )
        } else {
            return (
                <Layout title={"Companies"}>
                    {error.message}
                </Layout>
            )
        }
    }
}

export default withAuthSync(Companies);