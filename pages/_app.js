import App, { Container } from 'next/app';
import Head from 'next/head';

import 'semantic-ui-css/semantic.min.css'

import { Provider, observer } from 'mobx-react';
import DevTools from 'mobx-react-devtools';

import { initializeStore } from '../src/stores/stores';

@observer
class MyApp extends App {
  constructor(props) {
    super(props);
    const isServer = !process.browser
    this.mobxStore = isServer ? props.initialMobxState : initializeStore(props.initialMobxState.rootStore)
  }

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    const mobxStore = initializeStore({}, ctx);
    ctx.mobxStore = mobxStore;

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    
    const { isAuthenticated } = pageProps

    if (isAuthenticated) {
      await ctx.mobxStore.rootStore.personGroupStore.loadPersonGroups();
    }

    return {
      pageProps,
      initialMobxState: mobxStore
    }
  }

  render() {
    const { Component, pageProps, title } = this.props;
    return (
      <Container>
        <Head>
          <title>PRM {title ? `- ${title}` : ''}</title>
          {/* <link rel="stylesheet" href="//cdn.quilljs.com/1.2.6/quill.snow.css"></link> */}
          <link rel="stylesheet" href="//cdn.quilljs.com/1.2.6/quill.bubble.css"></link>
        </Head>
        <Provider {...this.mobxStore.rootStore}>
          <div>
            {process.env.NODE_ENV == 'development' ? <DevTools /> : ''}
            <Component {...pageProps} />
          </div>

        </Provider>
      </Container>
    );
  }
}

export default MyApp;