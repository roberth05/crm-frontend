// pages/about.js
import Layout from '../components/layout.js'
import React from 'react'
import { Header } from 'semantic-ui-react';

class AboutPage extends React.Component {

  render() {
    return (
      <Layout title="About">
        <Header>This is the about page</Header>
      </Layout>
    )
  }
}

export default AboutPage;