import Layout from '../components/layout.js'
import React from 'react'
import { withAuthSync } from '../src/auth';
import { Grid } from 'semantic-ui-react';
import { inject, observer } from 'mobx-react';

import TouchPointsList from '../components/touchpoints-list';
import PeopleToContactTable from '../components/people-toContact-table.js';
import ActionItemsList from '../components/actionItems-list.js';

@inject('uiStore', 'peopleStore', 'touchPointStore', 'actionItemStore')
@observer
class Index extends React.Component {
    static async getInitialProps(ctx) {
        await ctx.mobxStore.rootStore.personGroupStore.loadPersonGroups();
        await ctx.mobxStore.rootStore.peopleStore.loadPeople();
        await ctx.mobxStore.rootStore.touchPointStore.loadTouchPoints();
        await ctx.mobxStore.rootStore.actionItemStore.loadActionItems();
    }

    addTouchPointOnSave = async (data) => {
        try {
            let newTP = await this.props.touchPointStore.saveNew(data);
            if (data.actionItems.length != 0) {
                data.actionItems.forEach(actionItem => {
                    actionItem.setTouchPoint(newTP)
                    this.props.actionItemStore.saveNew(actionItem)
                });
            }
            this.props.uiStore.newTouchPointOpen = false;
        } catch (err) {
            throw err
        }
    };

    addActionItemOnSave = async (data) => {
        try {
            this.props.actionItemStore.saveNew(data);

            this.props.uiStore.newActionItemOpen = false;
        } catch (err) {
            throw err
        }
    };

    render() {
        const { isAuthenticated } = this.props;

        return (
            <Layout title={"Home Page"} isAuthenticated={isAuthenticated} loggedUser={this.props.username}>
                <Grid>
                    <Grid.Column width={10}>
                        <TouchPointsList
                            touchpoints={this.props.touchPointStore.sortedTouchPoints}
                            showPerson={true}
                            title="Latest Touch Points"
                            saveMethod={this.addTouchPointOnSave} />
                    </Grid.Column>
                    <Grid.Column width={6}>
                        <PeopleToContactTable
                            people={this.props.peopleStore.peopleToContact}
                        />
                        <ActionItemsList
                            actionItems={this.props.actionItemStore.sortedActionItems}
                            saveMethod={this.addActionItemOnSave}
                        />
                    </Grid.Column>
                </Grid>
            </Layout>
        )
    }
}

export default withAuthSync(Index);