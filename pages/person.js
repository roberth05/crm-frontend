import Layout from '../components/layout.js'
import React from 'react'
import { withAuthSync } from '../src/auth';

import { Image, Card, Button, Grid } from 'semantic-ui-react';

import SortableTable from "../components/sortable-table";
import Link from "next/link";
import TouchPointsList from '../components/touchpoints-list.js';
import PersonForm from '../components/person-form';
import { addNewTouchPoint } from '../src/api/touchpoints';
import { savePerson } from '../src/api/people';
import Person from '../src/model/person';
import TouchPoint from '../src/model/touchpoint.js';
import { formatDate } from '../src/utils.js';
import PersonGroup from '../src/model/person-group.js';
import { getPersonGroups } from '../src/api/person-groups.js';
import { inject, observer } from 'mobx-react';
import { ModalRoleForm } from '../components/forms/modal-role-form.js';

@inject('uiStore', 'personGroupStore', 'roleStore', 'peopleStore', 'companyStore')
@observer
class PersonPage extends React.Component {
    static async getInitialProps(context) {
        const { id } = context.query;

        await context.mobxStore.rootStore.personGroupStore.loadPersonGroups();
        await context.mobxStore.rootStore.companyStore.loadCompanies();
        const person = await context.mobxStore.rootStore.peopleStore.loadPerson(id);
        await context.mobxStore.rootStore.roleStore.loadRolesForPerson(person);
        await context.mobxStore.rootStore.touchPointStore.loadTouchPointsForPerson(person);
        

        // console.log(person)
        context.mobxStore.rootStore.uiStore.selectPerson(person.id);
        // console.log(context.mobxStore.rootStore.uiStore)


        //console.log(`person page type: ${person.constructor.name}`)
        return {
            person: person
        }
    }

    addTouchPointOnSave = async (data) => {
        try {
            //console.log(data)
            const response = await addNewTouchPoint(data);

            //console.log(response.person)
            const person = new Person(response.person);

            const touchpoint = new TouchPoint(response, person);
            this.setState({
                touchPoints: this.state.touchpoints.push(touchpoint)
            })
            this.props.uiStore.newTouchPointOpen = false;
        } catch (err) {
            throw err
        }
    };

    addRoleOnSave = async (data) => {
        try {
            // console.log(data)
            const response = await this.props.roleStore.saveNew(data);
            // console.log(response)
            // const role = new PersonRole(response.Personrole, this.props.uiStore.selectedPerson, new Company(response.Company))

            // this.props.roleStore.addRole(role);
            this.props.uiStore.newRoleOpen = false;
        } catch (err) {
            throw err
        }
    }

    editPersonOnSave = async (data) => {
        //console.log(`editPersonOnSave: ${JSON.stringify(data)}`)
        //console.log(`editPersonOnSave has forAPI: ${_.functionsIn(data)}`);
        try {
            const person = new Person(await savePerson(data));
            this.setState({
                person: person,
                isPersonEdit: false
            })
        } catch (err) {
            throw err
        }
    };

    componentWillUnmount() {
        this.props.uiStore.selectedPerson = null;
    }

    componentWillMount() {
        this.setState({
            isPersonEdit: false,
            roleColumns: [
                {
                    name: 'personRecord',
                    label: 'Person',
                    visible: false,
                    customRender: (value) => {
                        return (
                            <Link href={`/person?id=${value.id}`}>
                                <a>{`${value.FirstName} ${value.LastName}`}</a>
                            </Link>
                        );
                    }
                },
                {
                    name: 'title',
                    label: 'Title',
                    visible: true
                },
                {
                    name: "company",
                    label: "Company",
                    visible: true,
                    customRender: (value) => {
                        if (value) {
                            return (
                                <Link href={`/company?id=${value.id}`}>
                                    <a>{value.name}</a>
                                </Link>
                            );
                        }
                    }
                },
                {
                    name: 'started',
                    label: 'Started',
                    visible: true,
                    customRender: (value) => {
                        return value ? formatDate(value) : ''
                    }
                },
                {
                    name: 'ended',
                    label: 'Ended',
                    visible: true,
                    customRender: (value) => {
                        return value ? formatDate(value) : ''
                    }
                }
            ]
        })
    }

    showEditForm(event) {
        event.preventDefault()
        this.setState({ isPersonEdit: true })
    }

    renderPersonSection(person) {
        //console.log(`renderPersonSection: ${this.state.isPersonEdit}`)
        if (!this.state.isPersonEdit) {
            return (
                <Card.Content>
                    {person.imageUrl &&

                        <Image floated='right' size='small' src={`http://localhost:1337${person.imageUrl.url}`} />
                    }
                    <Card.Header>{`${person.firstName} ${person.lastName}`}</Card.Header>
                    <Card.Meta>{`Birthday: ${person.birthday ? formatDate(person.birthday) : ''}`}</Card.Meta>
                    <Card.Meta>{person.groups.map((group) => {
                        return group.name
                    }).join('; ')}</Card.Meta>
                    <Card.Meta>{`Contact By: ${formatDate(person.nextTouchPointBy)}`}</Card.Meta>
                    <Card.Description>
                        {`Comments: ${person.comments}`}
                    </Card.Description>
                    
                    <Button basic size='small' floated='right' href="#" onClick={this.showEditForm.bind(this)}>
                        Edit
                    </Button>
                </Card.Content>
            )
        } else {
            return (
                <PersonForm onSave={this.editPersonOnSave} person={this.state.person} title="Edit Person" />
            )
        }
    }

    handleRoleOpen = () => this.props.uiStore.newRoleOpen = true;

    render() {
        const { classes, error, isAuthenticated } = this.props;

        const person = this.props.uiStore.selectedPerson;
        //if (error) { console.log(`Error in render: ${JSON.stringify(error)}`) };
        //if (person) { console.log(`Person returned: ${JSON.stringify(person)}`) };
        //if (classes) { console.log(`Classes returned: ${JSON.stringify(classes)}`) };
        return (
            <Layout title={`${person.firstName} ${person.lastName}`} isAuthenticated={isAuthenticated} loggedUser={this.props.username}>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={10}>
                            <Card>
                                {this.renderPersonSection(person)}
                            </Card>
                        </Grid.Column>
                        <Grid.Column width={6} floated='right'>
                            <SortableTable
                                title="Roles"
                                headerControls={
                                    <ModalRoleForm
                                        open={this.props.newRoleOpen}
                                        saveMethod={this.addRoleOnSave}
                                        peopleList={this.props.peopleStore.people}
                                        companiesList={this.props.companyStore.companies}
                                        person={person}
                                        // role={new PersonRole('', '', '', '', this.props.uiStore.selectedPerson)}
                                        trigger={
                                            <Button compact color='green' basic icon='add' onClick={this.handleRoleOpen} />
                                        } />
                                }
                                data={person.sortedPersonRoles}
                                columns={this.state.roleColumns} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={12}>
                            <TouchPointsList
                                touchpoints={person.sortedTouchPoints}
                                showPerson={false}
                                saveMethod={this.addTouchPointOnSave}
                                touchpoint={{ person: person, Date: Date() }}
                                person={person}
                            />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Layout >
        )
    }
}

export default withAuthSync(PersonPage);