import Layout from '../components/layout.js'
import React from 'react'
import { withAuthSync } from '../src/auth';
import PeopleTable from '../components/people-table.js';
import { Grid, GridColumn } from 'semantic-ui-react';
import { inject, observer } from 'mobx-react';

@inject('uiStore', 'peopleStore', 'personGroupStore')
@observer
class People extends React.Component {
    static async getInitialProps(ctx) {
        await ctx.mobxStore.rootStore.personGroupStore.loadPersonGroups()
        await ctx.mobxStore.rootStore.peopleStore.loadPeople()
        await ctx.mobxStore.rootStore.touchPointStore.loadTouchPoints()
    }

    addPersonOnSave = async (data) => {
        //console.log(`addPersonOnSave: ${JSON.stringify(data)}`)
        try {
            this.props.peopleStore.saveNew(data);
            this.props.uiStore.newPersonOpen = false;
        } catch (err) {
            throw err
        }
    };

    render() {
        const { error, isAuthenticated } = this.props;
        if (error) { console.log(`Error in render: ${error}`) }
        //if (people) { console.log(`People returned: ${JSON.stringify(people)}`) };
        if (!error) {
            return (
                <Layout title={"People"} isAuthenticated={isAuthenticated} loggedUser={this.props.username}>
                    <Grid>
                        <GridColumn width={16}>
                            <PeopleTable people={this.props.peopleStore.sortedPeopleAsc} saveMethod={this.addPersonOnSave} />
                        </GridColumn>
                    </Grid>
                </Layout>
            )
        } else {
            return (
                <Layout title={"People"}>
                    {error.message}
                </Layout>
            )
        }
    }
}

export default withAuthSync(People);