/* /pages/signin.js */
import React from "react";
import Router from 'next/router';
import { Grid, Header, Image, Form, Button, Segment, Loader, Dimmer } from 'semantic-ui-react';
import { login } from '../src/auth';
import getRootUrl from '../src/api/getRootUrl';

import fetch from 'isomorphic-unfetch';

class SignIn extends React.Component {
    constructor(props) {
        super(props)

        this.state = { data: { username: '', password: '' }, error: '', loading: false }
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        if (this.props.isAuthenticated) {
            Router.push("/"); // redirect if you're already logged in
        }
    }

    handleChange(propertyName, event) {
        console.log(`HandleChange: ${propertyName}`);
        const { data } = this.state;
        data[propertyName] = event.target.value;
        this.setState({
            data: data,
            error: null
        });
    }

    async handleSubmit(event) {
        console.log(`Entered onSubmit`)
        event.preventDefault()

        this.setState({ loading: true });

        var body = {
            identifier: this.state.data.email,
            password: this.state.data.password
        }

        const response = await fetch(`${getRootUrl()}/auth/local`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body)
        });

        if (response.ok) {
            const { jwt, user } = await response.json()
            console.log(user)
            login({ jwt, user });
        } else {
            console.log('Login failed.')
            let error = new Error(response.statusText)
            error.response = response
            let message = await response.json();
            this.setState({ error: message.message, loading: false })
            //throw error
        }
    }

    render() {
        const { error } = this.state;
        return (
            <div className='login-form'>
                <style>{`
                body > div,
                body > div > div,
                body > div > div > div.login-form {
                    height: 100%;
                }
            `}
                </style>
                <Grid centered>
                    <Grid.Column style={{ maxWidth: 450, margin: '2em' }} color='violet'>
                        
                        <Image src="/static/Persy-logo.png" centered />
                        <Header as='h2' textAlign='center' inverted>
                            Log-in to your account
                        </Header>
                        <Form size="large" onSubmit={this.handleSubmit}>
                            <Segment stacked>
                                <Dimmer active={this.state.loading}>
                                    <Loader>Logging In</Loader>
                                </Dimmer>
                                <Form.Input
                                    fluid icon='user'
                                    iconPosition='left'
                                    placeholder='E-mail address'
                                    onChange={this.handleChange.bind(this, "email")} />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Password'
                                    type='password'
                                    onChange={this.handleChange.bind(this, "password")}
                                />

                                <Button fluid size='large'>
                                    Login
                                </Button>
                                {error &&
                                    <Segment color='red' inverted raised secondary>{error}</Segment>
                                }
                            </Segment>
                        </Form>
                    </Grid.Column >
                </Grid>
            </div>
        );
    }
}

export default SignIn;