import Layout from '../components/layout.js'
import React from 'react'
import { withAuthSync } from '../src/auth';
import CompanyInfoCard from '../components/company-info-card.js';
import { Grid } from 'semantic-ui-react';
import SortableTable from '../components/sortable-table'
import Link from 'next/link';

import { formatDate } from '../src/utils';
import { inject, observer } from 'mobx-react';

const roleColumns = [
    {
        name: 'person',
        label: 'Person',
        visible: true,
        customRender: (value) => {
            if (value) {
                return (
                    <Link href={`/person?id=${value.id}`}>
                        <a>{`${value.firstName} ${value.lastName}`}</a>
                    </Link>
                );
            }
        }
    },
    {
        name: 'company',
        label: 'Company',
        visible: false
    },
    {
        name: 'title',
        label: 'Title',
        visible: true
    },
    {
        name: 'started',
        label: 'Started',
        visible: true,
        customRender: (value) => {
            return value ? formatDate(value) : ''
        }
    },
    {
        name: 'ended',
        label: 'Ended',
        visible: true,
        customRender: (value) => {
            return value ? formatDate(value) : ''
        }
    }
]

@inject('uiStore', 'companyStore')
@observer
class CompanyPage extends React.Component {
    static async getInitialProps(context) {
        const { id } = context.query;
        await context.mobxStore.rootStore.personGroupStore.loadPersonGroups();

        const company = await context.mobxStore.rootStore.companyStore.loadCompany(id);
        await context.mobxStore.rootStore.peopleStore.loadPeople();
        await context.mobxStore.rootStore.roleStore.loadRolesForCompany(company);
        context.mobxStore.rootStore.uiStore.selectCompany(company.id);
        return {
            company: company
        }
    }

    componentWillUnmount() {
        this.props.uiStore.selectedCompany = null;
    }

    render() {
        const { error, isAuthenticated } = this.props;
        var company = this.props.uiStore.selectedCompany;

        //if (company) { console.log(`Company returned: ${JSON.stringify(company)}`) };
        return (
            <Layout title={`${company.name}`} isAuthenticated={isAuthenticated} loggedUser={this.props.username} >
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={6}>
                            <CompanyInfoCard company={company} />
                        </Grid.Column>
                        <Grid.Column width={12} style={{ marginTop: '1.5em' }}>
                            <SortableTable data={company.uniquePersonRoles} sortedColumn={"person"} columns={roleColumns} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Layout>
        )
    }
}

export default withAuthSync(CompanyPage);