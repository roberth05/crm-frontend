import { observable, action, computed, autorun } from 'mobx';

import { getCompanyList, addNewCompany, getCompany } from '../api/companies';
import Company from '../model/company';

class CompanyStore {
    @observable companies = [];
    rootStore;

    constructor(initialData = {}, rootStore = null) {
        // console.log(`companyStore constructor: ${initialData}`)
        initialData.companies ? this.updateCompanies(initialData.companies) : this.companies = [];
        this.rootStore = rootStore;

        //autorun(() => console.log(this.report));
    }

    toJSON() {
        return { companies: this.companies.map(company => company.toJSON()) }
    }

    find(id) {
        //TODO: This would be much cleaner with a findOrFetch approach.  Would allow auto loading of companies.
        // console.log(id)
        return this.companies.find((company) => {
            return company.id === id
        })
    }

    @action
    async loadCompanies() {
        try {
            const json = await getCompanyList(this.rootStore.commonStore.token);
            // console.log(json)
            this.updateCompanies(json)
            // markLoading(false)
        } catch (err) {
            console.error("Failed to load companies ", err)
        }
    }

    @action
    async loadCompany(id) {
        try {
            const json = await getCompany(id, this.rootStore.commonStore.token);
            // console.log(json)
            return this.updateCompany(json)
            // markLoading(false)
        } catch (err) {
            console.error("Failed to load companies ", err)
        }
    }

    updateCompanies(json) {
        // console.log(json)
        json.forEach(companyJson => {
            // console.log(companyJson)
            this.updateCompany(companyJson)
        })
    }

    updateCompany(companyJson) {
        // console.log(companyJson)
        const thisCompany = Company.fromJS(companyJson)
        
        this.addCompany(thisCompany)
        return thisCompany
    }

    async fetchAll(context) {
        const response = await getCompanyList(context);
        response.forEach(company => {
            this.addCompany(new Company(company));
        });
    }

    @action addCompany(company) {
        if (!this.companies.find((thisCompany) => thisCompany.id === company.id)) {
            this.companies.push(company);
        }
    }

    async saveNew(company) {
        try {
            console.log(`saveNew`)
            return this.updateCompany(await addNewCompany(company, this.rootStore.commonStore.token))
        } catch (err) {
            console.error('Failed to add new ', err)
        }
    }

    @computed get sortedCompanies() {
        return sortCompanies(this.companies)
    }
}

function sortCompanies(companies) {
    return companies.slice().sort((a, b) => {
        if (a.name < b.name)
            return -1;
        if (a.name > b.name)
            return 1;
        return 0;
    })
}
export default CompanyStore;