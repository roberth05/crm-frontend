import { observable, action, computed } from 'mobx';

import { getTouchPointsList, addNewTouchPoint, deleteTouchPoint as deleteTouchPointAPI, getTouchPointsForPerson } from '../api/touchpoints';
import TouchPoint from '../model/touchpoint';

class TouchPointStore {
    @observable touchPoints = [];
    rootStore;

    constructor(initialData = {}, rootStore = null) {
        // console.log(`touchPointStore constructor: ${JSON.stringify(initialData)}`)        
        this.rootStore = rootStore;

        initialData.touchPoints ? this.updateTouchPoints(initialData.touchPoints) : this.touchPoints = [];

        //autorun(() => console.log(this.report));
    }

    toJSON() {
        return { touchPoints: this.touchPoints.map(touchPoint => touchPoint.toJSON()) }
    }

    find(id) {
        return this.touchPoints.find((touchPoint) => {
            return touchPoint.id === id
        })
    }

    @action
    async loadTouchPoints() {
        try {
            const json = await getTouchPointsList(this.rootStore.commonStore.token);
            // console.log(json)
            this.updateTouchPoints(json)
            // markLoading(false)
        } catch (err) {
            console.error("Failed to load touchPoints ", err)
        }
    }

    @action
    async loadTouchPointsForPerson(person) {
        try {
            const json = await getTouchPointsForPerson(person, this.rootStore.commonStore.token);
            // console.log(json)
            this.updateTouchPoints(json)
            // markLoading(false)
        } catch (err) {
            console.error("Failed to load touchPoints for person ", err)
        }
    }

    updateTouchPoints(json) {
        // console.log(json)
        json.forEach(touchPointJson => {
            // console.log(touchPointJson)
            this.updateTouchPoint(touchPointJson)
        })
    }

    updateTouchPoint(touchPointJson) {
        // console.log(touchPointJson)
        const thisTouchPoint = TouchPoint.fromJS(touchPointJson)
        if (touchPointJson.person) {
            // console.log(touchPointJson.person)
            if (this.rootStore) {
                //console.log(this.rootStore.peopleStore.find(touchPointJson.person))
                thisTouchPoint.setPerson(this.rootStore.peopleStore.find(touchPointJson.person))
            }
        }
        this.addTouchPoint(thisTouchPoint)
        return thisTouchPoint
    }

    async fetchAll(context) {
        const response = await getTouchPointsList(context);
        response.forEach(touchPoint => {
            this.addTouchPoint(new TouchPoint(touchPoint));
        });
    }

    @action addTouchPoint(touchPoint) {
        if (!this.touchPoints.find((fp) => fp.id === touchPoint.id)) {
            this.touchPoints.push(touchPoint);
        }
    }

    async saveNew(touchPoint) {
        try {
            // console.log(`saveNew`)
            return this.updateTouchPoint(await addNewTouchPoint(touchPoint, this.rootStore.commonStore.token))
        } catch (err) {
            console.error('Failed to add new ', err)
        }
    }

    @action
    async deleteTouchPoint(id) {
        return deleteTouchPointAPI(id, this.rootStore.commonStore.token).then(
            action("Remove Deleted Id", response => {
                for (let index = 0; index < this.touchPoints.length; index++) {
                    if (this.touchPoints[index].id === id) {
                        this.touchPoints[index].clearPerson();
                        this.touchPoints.splice(index, 1);
                        index--;
                    }
                }
            })
        )
    }

    @computed get sortedTouchPoints() {
        return sortTouchPoints(this.touchPoints)
    }
}

//Sorts in descending order by default
function sortTouchPoints(touchPoints) {
    return touchPoints.slice().sort((a, b) => {
        return a.compare(b)
    })
}
export default TouchPointStore;