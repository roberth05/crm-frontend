import { observable, action, computed, autorun } from 'mobx';

import { getPersonGroups } from '../api/person-groups';
import PersonGroup from '../model/person-group';

class PersonGroupStore {
    @observable personGroups = [];
    rootStore;

    constructor(initialData = {}, rootStore = null) {
        initialData.personGroups ? this.updateGroups(initialData.personGroups) : [];
        this.rootStore = rootStore;
        //autorun(() => console.log(this.report));
    }

    //TODO: would be much cleaner as findOfFetch.  Would allow auto loading of groups without calling in advance.
    find(id) {
        return this.personGroups.find((group) => {
            return group.id === id
        })
    }
    
    @action updateGroups(groups) {
        groups.forEach((group) => {
            this.updateGroup(group)
        })
    }

    @action updateGroup(group) {
        const thisGroup = PersonGroup.fromJS(group)
        // console.log(thisGroup)
        this.addPersonGroup(thisGroup)
        return thisGroup
    }

    @action
    async loadPersonGroups() {
        try {
            const json = await getPersonGroups(this.rootStore.commonStore.token);
            // console.log(json)
            this.updateGroups(json)
            // markLoading(false)
        } catch (err) {
            console.error("Failed to load personGroups ", err)
        }
    }

    async fetchAll(context) {
        //console.log(`personGroupStore.fetchAll`)
        const response = await getPersonGroups(context);
        //console.log(`fetchAll response: ${response}`)
        response.forEach(group => {
            this.addPersonGroup(new PersonGroup(group));
        });
        //console.log(this.personGroups)
    }

    @action addPersonGroup(personGroup) {
        //console.log(`addPersonGroup: ${JSON.stringify(personGroup)}`)
        //console.log(`addPersonGroups: ${JSON.stringify(this.personGroups)}`)
        var group = this.personGroups.find(pg => pg.id === personGroup.id);
        if (!group) {
            //console.log(`Pushing group: ${JSON.stringify(group)}`)
            this.personGroups.push(personGroup);
        }
    }

    toJSON() {
        return { personGroups: this.personGroups.map(group => group.toJSON()) }
    }

    @computed get report() {
        return `groups: ${JSON.stringify(this.personGroups)}`
    }
}

export default PersonGroupStore;