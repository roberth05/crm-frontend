import { observable, action, computed } from "mobx";
import ActionItem from "../model/actionItem";
import { getActionItemList, addNewActionItem, deleteActionItem as deleteActionItemAPI, saveActionItem } from "../api/actionItems";

export default class ActionItemStore {
    @observable actionItems = [];
    rootStore;

    constructor(initialData = {}, rootStore = null) {
        this.rootStore = rootStore;
        initialData.actionItems ? this.updateActionItems(initialData.actionItems) : this.actionItems = [];
    }

    updateActionItems(items) {
        items.forEach(item => {
            this.updateActionItem(item);
        })
    }

    updateActionItem(item) {
        const thisItem = ActionItem.fromJS(item)
        if (this.rootStore && item.touchPoint) {
            console.log(item.touchPoint)
            thisItem.setTouchPoint(this.rootStore.touchPointStore.find(item.touchPoint))
        }
        this.addActionItem(thisItem)
        return thisItem
    }

    find(id) {
        return this.actionItems.find((actionItem) => {
            return actionItem.id === id
        })
    }

    @action async loadActionItems() {
        try {
            const json = await getActionItemList(this.rootStore.commonStore.token);
            this.updateActionItems(json)
        } catch (err) {
            console.error("Failed to load actionItems ", err)
        }
    }

    @action async saveNew(actionItem) {
        try {
            return this.updateActionItem(await addNewActionItem(actionItem, this.rootStore.commonStore.token))
        } catch (err) {
            console.error('Failed to add new actionItem ', err)
        }
    }

    @action async save(actionItem) {
        try {
            await saveActionItem(actionItem, this.rootStore.commonStore.token)
            return actionItem
        } catch (err) {
            console.error('Failed to save actionItem ', err)
        }
    }

    @action
    addActionItem(item) {
        if (!this.actionItems.find((thisItem) => thisItem.id === item.id)) {
            this.actionItems.push(item);
        }
    }

    @action
    deleteActionItem(id) {
        return deleteActionItemAPI(id, this.rootStore.commonStore.token).then(
            action("Remove Deleted ActionItem Id", response => {
                for (let index = 0; index < this.actionItems.length; index++) {
                    if (this.actionItems[index].id === id) {
                        this.actionItems.splice(index, 1);
                        index--;
                    }
                }
            })
        )
    }

    toJSON() {
        return { actionItems: this.actionItems.map(actionItem => actionItem.toJSON()) }
    }

    @computed get sortedActionItems() {
        return sortActionItems(this.actionItems)
    }
}

function sortActionItems(items) {
    return items.slice().sort((a, b) => {
        return a.compare(b);
    })
}