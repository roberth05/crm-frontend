import { useStaticRendering } from 'mobx-react';
import nextCookie from 'next-cookies';

import PersonGroupStore from './person-group-store';
import UIStore from './ui-store';
import PeopleStore from './people-store';
import RoleStore from './roleStore';
import CompanyStore from './company-store';
import CommonStore from './common-store';
import TouchPointStore from './touchPoint-store';
import ActionItemStore from './actionItem-store';

const isServer = typeof window === 'undefined';
useStaticRendering(isServer);

let rootStore = null;

export default class RootStore {
    constructor(initialData) {
        this.personGroupStore = new PersonGroupStore(initialData.personGroupStore, this)
        this.peopleStore = new PeopleStore(initialData.peopleStore, this)
        this.companyStore = new CompanyStore(initialData.companyStore, this)
        this.roleStore = new RoleStore(initialData.roleStore, this)
        this.uiStore = new UIStore(initialData.uiStore, this)
        this.commonStore = new CommonStore(initialData.commonStore, this)
        this.touchPointStore = new TouchPointStore(initialData.touchPointStore, this)
        this.actionItemStore = new ActionItemStore(initialData.actionItemStore, this)
    }

    toJSON() {
        return {
            personGroupStore: this.personGroupStore.toJSON(),
            peopleStore: this.peopleStore.toJSON(),
            companyStore: this.companyStore.toJSON(),
            roleStore: this.roleStore.toJSON(),
            uiStore: this.uiStore.toJSON(),
            commonStore: this.commonStore.toJSON(),
            touchPointStore: this.touchPointStore.toJSON(),
            actionItemStore: this.actionItemStore.toJSON()
        }
    }
}

export function initializeStore(initialData, context = null) {
    // console.log(initialData)
    if (isServer) {
        rootStore = new RootStore(initialData);
    }
    if (rootStore === null) {
        rootStore = new RootStore(initialData);
    };

    // console.log(rootStore)

    if (context) {
        const { jwt, username } = nextCookie(context)
        rootStore.commonStore.token = jwt
        rootStore.commonStore.username = username
    }
    // console.log(rootStore)
    return { rootStore: rootStore };
}