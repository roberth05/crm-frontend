import { observable } from "mobx";

export default class CommonStore {
    @observable userName;
    token;
    rootStore;

    constructor(initialData, rootStore) {
        this.rootStore = rootStore
        if (initialData) {
            this.token = initialData.token
            this.userName = initialData.userName
        } else {
            this.token = ''
            this.userName = ''
        }
    }

    toJSON() {
        return {
            token: this.token,
            userName: this.userName
        }
    }
}