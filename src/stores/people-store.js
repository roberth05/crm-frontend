import { observable, action, computed, autorun } from 'mobx';

import { getPeopleList, addPerson as addPersonAPI, getPerson, deletePerson as deletePersonAPI } from '../api/people';
import Person from '../model/person';
import PersonGroup from '../model/person-group';
import { addDays } from 'date-fns';

class PeopleStore {
    @observable people = [];
    rootStore;

    constructor(initialData = {}, rootStore = null) {
        this.rootStore = rootStore;
        initialData.people ? this.updatePeople(initialData.people) : this.people = [];
        //autorun(() => console.log(this.report));
    }

    find(id) {
        //TODO: This would be much cleaner with a findOrFetch approach.  Would allow auto loading of people.
        const retVal = this.people.find((person) => {
            return person.id === id
        })
        return retVal
    }

    updatePeople(json) {
        // console.log(json)
        json.forEach(personJson => {
            this.updatePerson(personJson)
        })
    }

    updatePerson(personJson) {
        let person = Person.fromJS(personJson)

        if (personJson.groups) {
            personJson.groups.forEach((group) => {
                person.addPersonGroup(this.rootStore.personGroupStore.find(group))
            })
        }

        //TODO: Need to handle roles
        this.addPerson(person)
        return person
    }

    @action
    async loadPeople() {
        try {
            const json = await getPeopleList(this.rootStore.commonStore.token);
            // console.log(json)
            this.updatePeople(json)
            // markLoading(false)
        } catch (err) {
            console.error("Failed to load people ", err)
        }
    }

    @action
    async loadPerson(id) {
        try {
            // console.log(id)
            const json = await getPerson(id, this.rootStore.commonStore.token);
            // console.log(json)
            return this.updatePerson(json)
            // markLoading(false)
        } catch (err) {
            console.error("Failed to load person ", err)
        }
    }

    async fetchAll(context) {
        const response = await getPeopleList(context);
        response.forEach(person => {
            this.addPerson(new Person(person));
        });
    }

    @action addPerson(person) {
        if (!this.people.find((fp) => fp.id === person.id)) {
            this.people.push(person);
        }
    }

    @action
    async saveNew(person) {
        try {
            // console.log(`saveNew`)
            return this.updatePerson(await addPersonAPI(person, this.rootStore.commonStore.token))
        } catch (err) {
            console.error('Failed to add new person ', err)
        }
    }

    @action
    async deletePerson(id) {
        return deletePersonAPI(id, this.rootStore.commonStore.token).then(
            action("Remove Deleted Person", response => {
                for (let index = 0; index < this.people.length; index++) {
                    if (this.people[index].id === id) {
                        this.people.splice(index, 1);
                        index--;
                    }
                }
            })
        )
    }

    //TODO: Should change this to be a computedFn from mobx-utils.  That would allow for argument stating direction of sort.
    @computed get sortedPeopleAsc() {
        return sortPeople(this.people, false)
    }

    @computed get sortedPeopleDesc() {
        return sortPeople(this.people, true)
    }

    @computed get peopleToContact() {
        return this.people.filter((person) => {
            return person.nextTouchPointBy <= addDays(new Date(), 5)
        }).sort((a, b) => {
            return a.nextTouchPointBy > b.nextTouchPointBy ? 1 : a.nextTouchPointBy < b.nextTouchPointBy ? -1 : 0
        })
    }

    toJSON() {
        return { people: this.people.map(person => person.toJSON()) };
    }

    @computed get report() {
        return `people: ${JSON.stringify(this.people)}`
    }
}

function sortPeople(people, descending = false) {
    return people.slice().sort((a, b) => {
        const lastNameComp = a.lastName.localeCompare(b.lastName)
        let retVal
        if (lastNameComp == 0) {
            retVal = a.firstName.localeCompare(b.firstName)
        } else {
            retVal = lastNameComp
        }
        if (descending) retVal = retVal * -1
        return retVal
    })
}
export default PeopleStore;