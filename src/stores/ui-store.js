import { observable, computed, autorun, action } from "mobx";
import { useStaticRendering } from 'mobx-react';
//import _ from 'lodash';

const isServer = !process.browser;
useStaticRendering(isServer);

export default class UIStore {
    @observable sidebarOpen = false;
    @observable newPersonOpen = false;
    @observable newTouchPointOpen = false;
    @observable newRoleOpen = false;
    @observable newActionItemOpen = false;
    @observable selectedPerson = null;
    @observable selectedCompany = null;
    rootStore = null;

    constructor(initialData, rootStore = null) {
        this.rootStore = rootStore;
        if (initialData) {
            this.sidebarOpen = initialData.sidebarOpen;
            this.newPersonOpen = initialData.newPersonOpen;
            this.newTouchPointOpen = initialData.newTouchPointOpen;
            this.newRoleOpen = initialData.newRoleOpen;
            this.newActionItemOpen=initialData.newActionItemOpen;
            initialData.selectedPerson ? this.selectPerson(initialData.selectedPerson) : null;
            initialData.selectedCompany ? this.selectCompany(initialData.selectedCompany) : null;
        }
    }

    @action selectPerson(personId) {
        this.selectedPerson = this.rootStore.peopleStore.find(personId);
    }

    @action selectCompany(companyId) {
        this.selectedCompany = this.rootStore.companyStore.find(companyId)
    }

    toJSON() {
        return {
            sidebarOpen: this.sidebarOpen,
            newPersonOpen: this.newPersonOpen,
            newTouchPointOpen: this.newTouchPointOpen,
            newRoleOpen: this.newRoleOpen,
            newActionItemOpen: this.newActionItemOpen,
            selectedPerson: this.selectedPerson ? this.selectedPerson.id : null,
            selectedCompany: this.selectedCompany ? this.selectedCompany.id : null
        }
    }
}

let uiStore = null;

export function initializeStore(initialData, rootStore = null) {
    if (isServer) {
        return new UIStore(initialData, rootStore)
    }

    if (uiStore === null) {
        uiStore = new UIStore(initialData, rootStore)
    }

    return uiStore;
}