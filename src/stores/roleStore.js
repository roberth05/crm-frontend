import { observable, action, computed, autorun } from 'mobx';

import { getRoles, getPersonRolesForPerson, getPersonRolesForCompany, addRole as addNewRoleAPI } from '../api/personRoles';
import PersonRole from '../model/person-role';
import Company from '../model/company';

class RoleStore {
    @observable roles = [];
    transportLayer = null;
    rootStore;

    constructor(initialData = {}, rootStore = null, transportLayer = null) {
        this.rootStore = rootStore;
        initialData.roles ? this.updateRoles(initialData.roles) : [];

        if (transportLayer) {
            this.transportLayer = transportLayer;
            this.getPersonRolesForPerson = transportLayer.getPersonRolesForPerson;
            this.getRoles = transportLayer.getRoles;
        }
        //autorun(() => console.log(this.report));
    }

    toJSON() {
        return { roles: this.roles.map(role => role.toJSON()) }
    }

    findByPerson(person) {
        return this.roles.filter((role) => { return role.person.id === person.id })
    }

    findByCompany(company) {
        return this.roles.filter((role) => { return role.company.id === company.id })
    }

    updateRoles(roles) {
        roles.forEach((role) => {
            this.updateRole(role)
        })
    }

    updateRole(role) {
        const thisRole = PersonRole.fromJS(role);
        if (role.person) thisRole.setPerson(this.rootStore.peopleStore.find(role.person))
        if (role.company) thisRole.setCompany(this.rootStore.companyStore.find(role.company))
        this.addRole(thisRole);
        return thisRole;
    }

    async fetchAll(context) {
        const response = await getRoles(context);
        response.forEach(role => {
            this.addRole(new PersonRole(role));
        });
    }

    @action
    async loadRoles() {
        const response = await getRoles(this.rootStore.commonStore.token);
        this.updateRoles(response)
    }

    @action
    async loadRolesForPerson(person) {
        const response = await getPersonRolesForPerson(person, this.rootStore.commonStore.token)
        // console.log(response)
        this.updateRoles(response)
    }

    @action
    async loadRolesForCompany(company) {
        const response = await getPersonRolesForCompany(company, this.rootStore.commonStore.token)
        this.updateRoles(response)
    }

    async fetchAllForPerson(person, context) {
        //console.log('fetchAllForPerson: before get');
        const response = await getPersonRolesForPerson(person, context);
        //console.log(response)
        response.forEach(role => {
            this.addRole(new PersonRole(role, person, new Company(role.company)));
        });
        //console.log(this.roles)
    }

    @action addRole(role) {
        //console.log(role)
        var foundRole = this.roles.find((searchRole) => searchRole.id === role.id)
        //console.log(this.roles)
        if (!foundRole) {
            this.roles.push(role);
        }
        //console.log(this.roles)
    }

    async saveNew(role) {
        try {
            // console.log(`saveNew`)
            return this.updateRole(await addNewRoleAPI(role, this.rootStore.commonStore.token))
        } catch (err) {
            console.error('Failed to add new ', err)
        }
    }

    @computed get report() {
        return `groups: ${JSON.stringify(this.roles)}`
    }

    @computed get sortedRoles() {
        return sortRoles(this.roles);
    }
}

function sortRoles(roles) {
    return roles.slice().sort((a, b) => {
        return a.compare(b)
    })
}

export default RoleStore;