import Cookies from "js-cookie";

export const getTokenFromServerCookie = req => {
    //console.log(`getTokenFromServerCookie.req: ${req}`);
    //console.log(`getTokenFromServerCookie.req.headers: ${JSON.stringify(req.headers)}`);
    if (!req.headers.cookie || "") {
      console.log(`Returning undefined getTokenFromServerCookie`);
      return undefined;
    }

    const jwtCookie = req.headers.cookie
      .split(";")
      .find(c => c.trim().startsWith("jwt="));
    if (!jwtCookie) {
      return undefined;
    }
    const jwt = jwtCookie.split("=")[1];
    return jwt;
  }

export const getTokenFromLocalCookie = () => {
    return Cookies.get("jwt");
  }

  var format = require('date-fns/format')

  var locales = {
    en: require('date-fns/locale/en'),
    es: require('date-fns/locale/es'),
    eo: require('date-fns/locale/eo')
  }
  
  export const formatDate = (date) => {
    return format(date, "M/D/YYYY", {
      locale: locales[global.__localeId__] // or global.__localeId__
    })
  }

  export const formatDateTime = (dateTime) => {
    return format(dateTime, "M/D/YYYY h:mm A", {
      locale: locales[global.__localeId__] // or global.__localeId__
    })
  }