import sendRequest, { getHeaders } from './sendRequest';

const BASE_PATH = '/actionItems';

export const actionItemForAPI = (actionItem, includeId = true) => {
    let returnVal = {
        description: actionItem.description,
        priority: actionItem.priority,
        due: actionItem.due,
        complete: actionItem.complete
    }

    if (includeId) {
        returnVal._id = actionItem.id
    }

    if (actionItem.touchPoint) {
        returnVal.touchpoint = actionItem.touchPoint.id
    }

    return returnVal
}

export const actionItemFromAPI = (actionItem) => {
    return {
        description: actionItem.description,
        priority: actionItem.priority,
        id: actionItem._id,
        due: actionItem.due,
        complete: actionItem.complete,
        touchPoint: actionItem.touchpoint ? actionItem.touchpoint._id : null
    }
}

export const getActionItemList = (context) => {
    return sendRequest(`${BASE_PATH}/`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(actionItems => {
        return actionItems.map(actionItem => actionItemFromAPI(actionItem))
    });
}

export const addNewActionItem = async (actionItem, context) => {
    return sendRequest(`${BASE_PATH}`, {
        method: 'POST',
        headers: getHeaders(context),
        body: JSON.stringify(actionItemForAPI(actionItem, false)),
    }).then(result => { return actionItemFromAPI(result) });
}

export const saveActionItem = async (actionItem, context) => {
    return sendRequest(`${BASE_PATH}/${actionItem.id}`, {
        method: 'PUT',
        headers: getHeaders(context),
        body: JSON.stringify(actionItemForAPI(actionItem, true))
    }).then(result => { return actionItemFromAPI(result) })
}

export const deleteActionItem = async (id, context) => {
    return sendRequest(`${BASE_PATH}/${id}`, {
        method: 'DELETE',
        headers: getHeaders(context)
    });
}