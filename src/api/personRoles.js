import sendRequest, { getHeaders } from './sendRequest';

const BASE_PATH = '/Personroles';

export const roleFromAPI = (role) => {
    let retVal = {
        id: role._id,
        title: role.Title,
        started: role.Started,
        ended: role.Ended,
        person: role.person ? role.person._id : null,
        company: role.company ? role.company._id : null
    }

    return retVal
}

export const roleForAPI = (role, includeId = true) => {
    // console.log(role)

    let retVal = {
        Title: role.title,
        Started: role.started,
        Ended: role.ended,
        person: role.person ? role.person.id : null,
        company: role.company ? role.company.id : null
    }
    if (includeId) {
        retVal._id = role.id
    }

    console.log(retVal)
    return retVal
}

export const getPersonRolesForPerson = (person, context) => {
    return sendRequest(`${BASE_PATH}?person=${person.id}`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(roles => {
        return roles.map((role) => { return roleFromAPI(role) });
    });
}

export const getPersonRolesForCompany = (company, context) => {
    return sendRequest(`${BASE_PATH}?company=${company.id}`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(roles => {
        return roles.map((role) => { return roleFromAPI(role) });
    });
}

export const getRoles = (context) => {
    return sendRequest(`${BASE_PATH}/`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(roles => {
        return roles.map((role) => { return roleFromAPI(role) });
    });
}

export const addRole = async (data, context) => {
    // console.log(`addRole: ${JSON.stringify(data)}`)
    const submission = JSON.stringify(roleForAPI(data, false))
    // console.log(`Role submission: ${submission}`)
    return  sendRequest(`${BASE_PATH}`, {
        method: 'POST',
        body: submission,
        headers: getHeaders(context)
    }).then((response)=> {
        return roleFromAPI(response);
    })
}