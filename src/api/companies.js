import sendRequest, { getHeaders } from './sendRequest';

const BASE_PATH = '/companies';

export const companyForAPI = (company) => {
    return {
        Name: company.name,
        Website: company.website,
        _id: company.id,
        comments: company.comments
    }
}
export const companyFromAPI = (company) => {
    return {
        name: company.Name,
        website: company.website,
        id: company.id,
        comments: company.comments
    }
}

export const getCompanyList = (context) => {
    return sendRequest(`${BASE_PATH}/`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(companies => {
        return companies.map(company => companyFromAPI(company))
    });
}

export const getCompany = async (id, context) => {
    return sendRequest(`${BASE_PATH}?_id=${id}`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(data => {
        return companyFromAPI(data[0]);
    }).catch(error => {
        Promise.reject(error);
    });
}

export const addNewCompany = async (company, context) => {
    return sendRequest(`${BASE_PATH}`, {
        method: 'POST',
        headers: getHeaders(context),
        body: JSON.stringify(companyForAPI(company)),
    }).then(result => { return companyFromAPI(result) });
}