export default function getRootUrl() {
  const port = process.env.PORT || 1337;
  const dev = process.env.NODE_ENV !== 'production';
  //const ROOT_URL = dev ? `http://localhost:${port}` : 'https://hammond-crm.herokuapp.com';
  const ROOT_URL = 'https://hammond-crm.herokuapp.com';

  return ROOT_URL;
}