import sendRequest, { getHeaders } from './sendRequest';

const BASE_PATH = '/Persongroups';

export const personGroupFromAPI = (group) => {
    return {
        id: group._id,
        name: group.Name,
        contactFrequency: group.contactFrequency
    }
}

export const personGroupForAPI = (group) => {
    //Should also serialize person, if it exists
    return {
        _id: group.id,
        Name: group.name,
        contactFrequency: group.contactFrequency
    }
}

export const getPersonGroups = (context) => {
    return sendRequest(`${BASE_PATH}/`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then((groups) => {
        return groups.map((group) => { return personGroupFromAPI(group) })
    }).catch((err) => { throw err })
}

export const savePersonGroup = (data, context) => {
    const { id } = data;
    console.log(`savePersonGroup: ${JSON.stringify(data)}`)
    return sendRequest(`${BASE_PATH}/${id}`, {
        method: 'PUT',
        body: JSON.stringify(personGroupForAPI(data)),
        headers: getHeaders(context)
    })
}