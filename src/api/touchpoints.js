import sendRequest, { getHeaders } from './sendRequest';

const BASE_PATH = '/touchpoints';

export const touchPointForAPI = (touchPoint, includeId = true) => {
    let retVal = {
        Method: touchPoint.method,
        Discussed: touchPoint.discussed,
        Date: touchPoint.date,
        InitiatedByMe: touchPoint.initiatedByMe,
        person: touchPoint.person.id
    }
    if (includeId) retVal._id = touchPoint.id
    return retVal

}
export const touchPointFromAPI = (touchPoint) => {
    return {
        method: touchPoint.Method,
        discussed: touchPoint.Discussed,
        id: touchPoint._id,
        date: touchPoint.Date,
        initiatedByMe: touchPoint.InitiatedByMe,
        person: touchPoint.person ? touchPoint.person._id : null
    }
}

export const getTouchPointsList = async (context) => {
    return sendRequest(`${BASE_PATH}?_sort=Date:DESC`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(touchpoints => {
        return touchpoints.map((touchPoint) => { return touchPointFromAPI(touchPoint) })
    });
}

export const getTouchPointsForPerson = async (person, context) => {
    return sendRequest(`${BASE_PATH}?person=${person.id}&_sort=Date:DESC`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(touchpoints => {
        return touchpoints.map((touchPoint) => { return touchPointFromAPI(touchPoint) })
    });
}

export const addNewTouchPoint = async (touchpoint, context) => {
    return sendRequest(`${BASE_PATH}`, {
        method: 'POST',
        headers: getHeaders(context),
        body: JSON.stringify(touchPointForAPI(touchpoint, false)),
    }).then((response) => {
        return touchPointFromAPI(response)
    });
}

export const deleteTouchPoint = async (id, context) => {
    return sendRequest(`${BASE_PATH}/${id}`, {
        method: 'DELETE',
        headers: getHeaders(context)
    });
}