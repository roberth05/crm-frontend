import 'isomorphic-unfetch';
import getRootUrl from './getRootUrl';
import nextCookie from 'next-cookies';

export const getHeaders = (context) => {
  let jwt
  try {
    jwt = nextCookie(context).jwt
  } catch (error) {
    //console.log(`caught error: ${error} - ${context}`)
    jwt = context
  }
  return {
    Authorization: `Bearer ${jwt}`
  }
}


export default async function sendRequest(path, opts = {}) {
  const headers = Object.assign({}, opts.headers || {}, {
    'Content-type': 'application/json; charset=UTF-8',
  });
  //console.log(`sendRequest headers: ${JSON.stringify(headers)}`)
  const response = await fetch(
    `${getRootUrl()}${path}`,
    Object.assign({ method: 'POST', credentials: 'same-origin' }, opts, { headers }),
  );

  //console.log(response)
  const data = await response.json();

  if (data.error) {
    throw new Error(data.error);
  }

  return data;
}