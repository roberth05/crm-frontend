import sendRequest, { getHeaders } from './sendRequest';

const BASE_PATH = '/people';

export const personForAPI = (person) => {
    //console.log(`person.forAPI: ${Object.getOwnPropertyNames(this.groups[0])}`)
    var groups = person.groups.map((group) => {
        // console.log(group)
        return group.id
    });

    const retVal = {
        FirstName: person.firstName,
        LastName: person.lastName,
        _id: person.id,
        Birthday: person.birthday,
        Comments: person.comments,
        persongroups: groups
    }
    return retVal
}

export const personFromAPI = (json) => {
    let retVal = {
        firstName: json.FirstName,
        lastName: json.LastName,
        id: json._id,
        birthday: json.Birthday,
        comments: json.Comments,
        imageUrl: json.Image,
    }
    if (json.touchpoints && json.touchpoints.length > 0) {
        //console.log(json.touchpoints);
        retVal.touchPoints = json.touchpoints.map(tp => {
            return tp._id
        })
    }
    // console.log(json)
    if (json.persongroups && json.persongroups.length > 0) {
        retVal.groups = json.persongroups.map(group => {
            return group._id
        })
    }
    return retVal
}

export const getPeopleList = async (context) => {
    return sendRequest(`${BASE_PATH}/`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(people => {
        return people.map((person) => { return personFromAPI(person) });
    }).catch(err => {
        // console.log(`getPeopleList err: ${err}`)
        throw err
    });
}

export const getPerson = (id, context) => {

    return sendRequest(`${BASE_PATH}?_id=${id}`, {
        method: 'GET',
        headers: getHeaders(context)
    }).then(data => {
        let thisPerson = personFromAPI(data[0]);
        return thisPerson;
    });
}

export const savePerson = (data, context) => {
    const { id } = data;
    console.log(`savePerson: ${JSON.stringify(data)}`)
    const submission = JSON.stringify(personForAPI(data))
    console.log(`Person submission: ${submission}`)
    return sendRequest(`${BASE_PATH}/${id}`, {
        method: 'PUT',
        body: JSON.stringify(personForAPI(data)),
        headers: getHeaders(context)
    })
}

export const addPerson = async (data, context) => {
    console.log(`addPerson: ${JSON.stringify(data)}`)
    const submission = JSON.stringify(personForAPI(data))
    console.log(`Person submission: ${submission}`)
    const response = sendRequest(`${BASE_PATH}`, {
        method: 'POST',
        body: submission,
        headers: getHeaders(context)
    }).then((res) => {
        return personFromAPI(res);
    })

    return response;
}

export const deletePerson = (id, context) => {
    return sendRequest(`${BASE_PATH}/${id}`, {
        method: 'DELETE',
        headers: getHeaders(context)
    });
}