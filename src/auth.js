import { Component } from 'react'
import Router from 'next/router'
import nextCookie from 'next-cookies'
import cookie from 'js-cookie'

export const login = async ({ jwt, user }) => {
    cookie.set('jwt', jwt)
    cookie.set('username', user.username)
    Router.push('/index')
}

export const logout = () => {
    cookie.remove('jwt')
    cookie.remove('username')
    // to support logging out from all windows
    window.localStorage.setItem('logout', Date.now())
    Router.push('/login')
}

// Gets the display name of a JSX component for dev tools
const getDisplayName = Component =>
    Component.displayName || Component.name || 'Component'

export const withAuthSync = WrappedComponent =>
    class extends Component {
        static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`

        static async getInitialProps(ctx) {
            const {jwt, username} = auth(ctx)
            //console.log(`auth.getInitialProps jwt: ${jwt}`);
            const componentProps =
                WrappedComponent.getInitialProps &&
                (await WrappedComponent.getInitialProps(ctx))

            const isAuthenticated = !!username
            return { ...componentProps, jwt, username, isAuthenticated }
        }

        constructor(props) {
            super(props)

            this.syncLogout = this.syncLogout.bind(this)
        }

        componentDidMount() {
            window.addEventListener('storage', this.syncLogout)
        }

        componentWillUnmount() {
            window.removeEventListener('storage', this.syncLogout)
            window.localStorage.removeItem('logout')
        }

        syncLogout(event) {
            if (event.key === 'logout') {
                console.log('logged out from storage!')
                Router.push('/login')
            }
        }

        render() {
            return <WrappedComponent {...this.props} />
        }
    }

export const auth = ctx => {
    const { jwt, username } = nextCookie(ctx)

    /*
     * This happens on server only, ctx.req is available means it's being
     * rendered on server. If we are on server and token is not available,
     * means user is not logged in.
     */
    //console.log(`auth.auth jwt: ${jwt}`)
    if (ctx.req && !jwt) {
        ctx.res.writeHead(302, { Location: '/login' })
        ctx.res.end()
        return
    }

    // We already checked for server. This should only happen on client.
    if (!jwt) {
        Router.push('/login')
    }

    return { jwt, username }
}