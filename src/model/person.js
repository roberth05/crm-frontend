import _ from 'lodash'
import { observable, action, computed } from 'mobx';
import addDays from 'date-fns/add_days';

// var parseDate = require('date-fns/parse')

export default class Person {
    @observable firstName = '';
    @observable lastName = '';
    @observable birthday = null;
    @observable comments = '';
    id = '';
    @observable imageUrl = '';
    @observable touchPoints = [];
    @observable personRoles = [];
    @observable groups = [];

    constructor(id, firstName, lastName, birthday, comments, imageUrl, groups = []) {
        this.id = id
        this.firstName = firstName ? firstName : ''
        this.lastName = lastName ? lastName : ''
        this.birthday = birthday ? birthday : null
        this.comments = comments ? comments : ''
        this.imageUrl = imageUrl ? imageUrl : ''
        this.groups = groups;
    }

    @action addPersonGroup(group) {
        if (!this.groups.find((currentGroup) => group.id === currentGroup.id)) {
            this.groups.push(group);
        }
    }

    @action addPersonRole(role) {
        if (!this.personRoles.find((currentRole) => role.id === currentRole.id)) {
            this.personRoles.push(role);
        }
    }

    @action addTouchPoint(touchPoint) {
        if (!this.touchPoints.find((currentTP) => touchPoint.id === currentTP.id)) {
            this.touchPoints.push(touchPoint);
        }
    }

    @action removeTouchPoint(touchPoint) {
        for (let index = 0; index < this.touchPoints.length; index++) {
            if (this.touchPoints[index].id === touchPoint.id) {
                console.log(`Removing: ${index}`)
                this.touchPoints.splice(index, 1);
                index--;
            }
        }
    }

    @computed get sortedPersonRoles() {
        return this.personRoles.slice().sort((a, b) => {
            return a.compare(b)
        })
    }

    @computed get sortedTouchPoints() {
        return this.touchPoints.slice().sort((a, b) => {
            return a.compare(b)
        })
    }

    @computed get latestTouchPoint() {
        return this.sortedTouchPoints[0]
    }

    @computed get latestTouchPointDate() {
        if (this.latestTouchPoint) return this.latestTouchPoint.date
    }

    @computed get sortedPersonGroups() {
        return this.groups.slice().sort((a, b) => {
            return a.compare(b)
        })
    }

    @computed get nextTouchPointBy() {
        const daysToAdd = this.sortedPersonGroups[0] ? this.sortedPersonGroups[0].contactFrequency : 30;
        let baseDate = this.latestTouchPoint ? new Date(this.latestTouchPoint.date) : new Date();
        return addDays(baseDate, daysToAdd);
    }

    toJSON() {
        let retVal = {
            firstName: this.firstName,
            lastName: this.lastName,
            id: this.id,
            birthday: this.birthday,
            comments: this.comments
        }
        //TODO: This is not serializing correctly.
        retVal.groups = this.groups.map((group) => { return group.id })
        return retVal
    }

    static fromJS(person) {
        return new Person(person.id, person.firstName, person.lastName, person.birthday, person.comments, person.imageUrl)
    }
}