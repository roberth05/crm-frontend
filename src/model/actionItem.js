import { observable, action } from "mobx";

var compareAsc = require('date-fns/compare_asc');

export const priorities = {
    HIGH: 100,
    NORMAL: 50,
    LOW: 10
}

export default class ActionItem {
    @observable description = '';
    @observable priority = priorities.NORMAL;
    @observable due = null;
    @observable complete = false;
    id;

    @observable touchPoint = null;

    constructor(id, description = '', priority = priorities.NORMAL, due = null, complete = false, touchPoint = null) {
        this.id = id;
        this.description = description;
        this.priority = priority;
        this.due = due;
        this.complete = complete;
        this.touchPoint = touchPoint;
    }

    @action
    toggleComplete() {
        this.complete = !this.complete
    }

    @action
    setTouchPoint(touchPoint) {
        this.touchPoint = touchPoint
        if (touchPoint) touchPoint.addActionItem(this)
    }

    @action
    clearTouchPoint() {
        if (this.touchPoint) {
            this.touchPoint.removeActionItem(this);
            this.touchPoint = null;
        }
    }

    toJSON() {
        return {
            id: this.id,
            description: this.description,
            priority: this.priority,
            due: this.due,
            complete: this.complete,
            touchPoint: this.touchPoint ? this.touchPoint.id : null
        }
    }

    //Should sort earliest due date first, then highest priority
    compare(otherAction) {
        if (this.due && otherAction.due) {
            const result = compareAsc(this.due, otherAction.due)
            if (result == 0) {
                if (this.priority > otherAction.priority) return -1
                if (this.priority < otherAction.priority) return 1
            }
            return result;
        }
        if (this.due) {
            return -1
        }
        if (otherAction.due) {
            return 1
        }
        return 0
    }

    static fromJS(json) {
        return new ActionItem(json.id, json.description, json.priority, json.due, json.complete)
    }
}

