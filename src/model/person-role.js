import { observable, action } from 'mobx';


export default class PersonRole {
    id = '';
    @observable title = '';
    @observable started = '';
    @observable ended = '';
    @observable person = null;
    @observable company = null;

    constructor(id, title, started, ended, person, company) {
        this.id = id
        this.title = title
        this.started = started
        this.ended = ended

        this.person = person ? person : null
        this.company = company ? company : null
    }

    toJSON() {
        return {
            id: this.id,
            title: this.title,
            started: this.started,
            ended: this.ended,
            person: this.person ? this.person.id : null,
            company: this.company ? this.company.id : null
        }
    }

    @action setPerson(person) {
        this.person = person
        this.person.addPersonRole(this)
    }

    @action setCompany(company) {
        this.company = company
        this.company.addPersonRole(this)
    }

    compare(otherRole) {
        if (this.started > otherRole.started)
            return -1;
        if (this.started < otherRole.started)
            return 1;
        if (this.ended > otherRole.ended)
            return -1;
        if (this.ended < otherRole.ended)
            return 1;
        return 0;
    }

    static fromJS(json) {
        return new PersonRole(json.id, json.title, json.started, json.ended)
    }
}