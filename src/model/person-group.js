export default class PersonGroup {
    name = '';
    id = '';
    contactFrequency = 0;

    constructor(id, name, contactFrequency) {
        this.name = name;
        this.id = id;
        this.contactFrequency = contactFrequency
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name,
            contactFrequency: this.contactFrequency
        }
    }

    compare(otherGroup) {
        if (this.contactFrequency > otherGroup.contactFrequency) {
            return 1
        }
        if (this.contactFrequency < otherGroup.contactFrequency) {
            return -1
        }
        if (this.contactFrequency === otherGroup.contactFrequency) {
            return this.name.localeCompare(otherGroup.name)
        }
    }

    static fromJS(groupJson) {
        return new PersonGroup(groupJson.id, groupJson.name, groupJson.contactFrequency)
    }
}