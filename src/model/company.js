import { observable, action, computed } from 'mobx';
import * as _ from 'lodash';

export default class Company {
    @observable name = '';
    @observable website = '';
    id = '';
    @observable comments = '';
    @observable personRoles = [];

    constructor(id, name, website, comments) {
        this.id = id
        this.name = name
        this.website = website
        this.comments = comments
    }

    toJSON() {
        return {
            id: this.id,
            name: this.name,
            website: this.website,
            comments: this.comments,
            personRoles: this.personRoles.map((role) => { return role.id })
        }
    }

    @action addPersonRole(role) {
        if (!this.personRoles.find((currentRole) => role.id === currentRole.id)) {
            this.personRoles.push(role);
        }
    }

    @computed get sortedPersonRoles() {
        return this.personRoles.slice().sort((a, b) => {
            return a.compare(b)
        })
    }

    @computed get uniquePersonRoles() {
        return _.sortedUniqBy(this.sortedPersonRoles, role => role.person.id)
    }

    static fromAPI(json) {
        return new Company(json._id, json.Name, json.Website, json.comments)
    }

    static fromJS(json) {
        return new Company(json.id, json.name, json.website, json.comments)
    }
}