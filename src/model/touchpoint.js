import { observable, action } from 'mobx';

var parseDate = require('date-fns/parse')

export default class TouchPoint {
    @observable method = '';
    @observable discussed = '';
    @observable date = '';
    @observable person = null;
    id = '';
    @observable initiatedByMe = false;
    @observable actionItems = [];

    constructor(id, method, discussed, date, initiatedByMe, person) {
        //console.log(json)
        this.method = method;
        this.discussed = discussed;
        this.id = id;
        this.date = date;
        this.initiatedByMe = (typeof initiatedByMe === 'undefined' || initiatedByMe === null) ? false : initiatedByMe;
        if (person) {
            this.person = person
        }
    }

    @action
    setPerson(person) {
        this.person = person
        this.person.addTouchPoint(this)
    }

    @action
    clearPerson() {
        if (this.person) {
            this.person.removeTouchPoint(this);
            this.person = null;
        }
    }

    @action addActionItem(actionItem) {
        if (!this.actionItems.find((currentAction) => actionItem.id === currentAction.id)) {
            this.actionItems.push(actionItem);
        }
    }

    @action removeActionItem(actionItem) {
        for (let index = 0; index < this.actionItems.length; index++) {
            if (this.actionItems[index].id === actionItem.id) {
                console.log(`Removing: ${index}`)
                this.actionItems.splice(index, 1);
                index--;
            }
        }
    }

    toJSON() {
        return {
            id: this.id,
            method: this.method,
            discussed: this.discussed,
            date: this.date,
            initiatedByMe: this.initiatedByMe,
            person: this.person ? this.person.id : null
        }
    }

    compare(otherTouchPoint) {
        if (this.date > otherTouchPoint.date)
            return -1;
        if (this.date < otherTouchPoint.date)
            return 1;
        return 0;
    }

    static fromAPI(json) {
        return new TouchPoint(json._id, json.Method, json.discussed, json.Date, json.initiatedByMe)
    }

    static fromJS(json) {
        return new TouchPoint(json.id, json.method, json.discussed, json.date, json.initiatedByMe)
    }
}