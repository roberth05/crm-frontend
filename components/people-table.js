import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import SortableTable from './sortable-table';
import { formatDate } from '../src/utils';
import { deletePerson } from '../src/api/people';
import { Icon, Confirm, Button } from 'semantic-ui-react';
import { ModalPersonForm } from './modal-person-form';
import { inject, observer } from 'mobx-react';

@inject('uiStore', 'peopleStore')
@observer
export default class PeopleTable extends React.Component {

    static propTypes = {
        people: PropTypes.array.isRequired,
    };

    componentWillMount() {
        this.setState({
            confirmOpen: {},
            newPersonOpen: false,
            columns: [
                {
                    name: "groups",
                    label: "Groups",
                    visible: true,
                    customRender: (value) => {
                        var display = value.map((group) => {
                            return group.name
                        }).join('; ')
                        return display
                    }
                },
                {
                    name: "firstName",
                    label: "First Name",
                    visible: true
                },
                {
                    name: "lastName",
                    label: "Last Name",
                    visible: true
                },
                {
                    name: "birthday",
                    label: "Birthday",
                    visible: true,
                    customRender: (value) => {
                        return (value ? formatDate(value) : null)
                    }
                },
                {
                    name: "latestTouchPointDate",
                    label: "Latest Touch Point",
                    visible: true,
                    customRender: (value) => {
                        if (value) return (formatDate(value))
                    }
                },
                {
                    name: "nextTouchPointBy",
                    label: "Next Touch Point",
                    visible: true,
                    customRender: (value) => {
                        if (value) return (formatDate(value))
                    }
                },
                {
                    name: "id",
                    label: "Controls",
                    visible: true,
                    textAlign: "center",
                    customRender: (value) => {
                        let stateName = `confirmOpen-${value}`
                        return (
                            <Fragment>
                                <Link href={`/person?id=${value}`}>
                                    <Icon link name='edit outline' />
                                </Link>
                                <Icon link color='red' name='trash alternate outline' onClick={() => this.show(stateName)} />
                                <Confirm
                                    open={this.state[stateName]}
                                    onCancel={() => this.handleCancel(stateName)}
                                    onConfirm={() => this.onRowDelete(value)} />
                            </Fragment>
                        );
                    }
                },
            ]
        })
    }

    handleCancel = (name) => this.setState({ [name]: false })

    show = (name) => this.setState({ [name]: true })

    onRowDelete(rowToDelete) {
        console.log(`Deleting ${JSON.stringify(rowToDelete)}`)
        try {
            this.props.peopleStore.deletePerson(rowToDelete)
            this.setState({
                [`confirmOpen-${rowToDelete}`]: false
            })
        } catch (error) {

        }
        console.log(rowToDelete, "were deleted!");
    }

    handlePersonOpen = () => this.props.uiStore.newPersonOpen = true;

    headerControls = (saveMethod) => {
        return (
            <ModalPersonForm saveMethod={saveMethod} open={this.props.uiStore.newPersonOpen} trigger={
                <Button compact color='green' basic icon='add' onClick={this.handlePersonOpen} />
            } />
        )
    }

    render() {
        return (
            <Fragment>
                <SortableTable
                    title="People"
                    data={this.props.people}
                    columns={this.state.columns}
                    headerControls={this.headerControls(this.props.saveMethod)}
                />
            </Fragment>
        )
    }
}