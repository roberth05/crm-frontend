import Person from "../src/model/person";
import { Modal, Header, Button, Icon } from "semantic-ui-react";
import PersonForm from "./person-form";
import { observer, inject } from "mobx-react";
import PropTypes from 'prop-types';

@inject('uiStore')
@observer
export class ModalPersonForm extends React.Component {
    static propTypes = {
        saveMethod: PropTypes.func.isRequired,
    };

    state={
        person: null,
        open: false
    }

    constructor(props) {
        super(props)
        this.state = {
            person: props.person ? props.person : new Person,
            open: props.open
        }
    }

    handlePersonCancel = () => {
        console.log(`Closing form`)
        this.props.uiStore.newPersonOpen=false;
    }

    handlePersonOpen = () => {this.props.uiStore.newPersonOpen=true}

    render() {
        return (
            <Modal
                open={this.props.uiStore.newPersonOpen}
                onClose={this.handlePersonCancel}
                onOpen={this.handlePersonOpen}
                size='small'
                trigger={this.props.trigger}
            >
                <Header icon='browser' content={this.props.title ? this.props.title : 'Add New Person'} />
                <Modal.Content>
                    <PersonForm
                        onSave={this.props.saveMethod}
                    />
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red' onClick={this.handlePersonCancel}>
                        <Icon name='cancel' />Cancel
                </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}