import React from 'react';
import PropTypes from 'prop-types';

import { Form, Header, Button } from 'semantic-ui-react';
import Company from '../src/model/company';

class CompanyForm extends React.Component {
    static propTypes = {
        onSave: PropTypes.func.isRequired,
    };

    static defaultProps = {
        company: null,
    };

    constructor(props) {
        super(props);

        this.state = {
            company: props.company ? props.company : new Company()
        };
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.props.onSave(this.state.company);
    };

    handleChange = (e, { name, value }) => {
        this.setState({
            company: Object.assign(this.state.company, { [name]: value })
        })
    }

    render() {
        return (
            <div>
                <Header as="h3">{this.props.title ? this.props.title : "Add New Company"}</Header>
                <Form size='large' onSubmit={this.onSubmit}>
                    <Form.Input
                        placeholder='Name'
                        value={this.state.company.name}
                        name='name'
                        onChange={this.handleChange}
                        required
                    />
                    <Form.Input
                        placeholder='Website'
                        value={this.state.company.website}
                        name='website'
                        onChange={this.handleChange}
                        required
                    />

                    <Form.TextArea
                        onChange={this.handleChange}
                        name='comments'
                        value={this.state.company.comments}
                        placeholder='Comments'
                    />
                    <Button variant="contained" type="submit">
                        Save
                    </Button>
                </Form>
            </div >
        );
    }
}

export default CompanyForm;