import React, { Fragment } from 'react';
import Link from 'next/link';
import SortableTable from './sortable-table';
import { formatDateTime } from '../src/utils';
import { Icon, Confirm, Button, Checkbox } from 'semantic-ui-react';
import { observer, inject } from 'mobx-react';
import { ModalActionItemForm } from './forms/modal-actionItem-form';

@inject('actionItemStore')
@observer
class ActionItemsList extends React.Component {
    constructor(props) {
        super(props)
        if (typeof window !== "undefined") {
            this.ReactQuill = require("react-quill");
        }
    }

    ReactQuill = null;

    componentWillMount() {
        this.setState({
            title: this.props.title || "Action Items",
            confirmOpen: {},
            actionItemColumns: [
                {
                    name: "id",
                    label: "Id",
                    visible: false
                },
                {
                    name: "due",
                    label: "Due Date",
                    visible: true,
                    customRender: (value) => {
                        return formatDateTime(value)
                    }
                },
                {
                    name: "priority",
                    label: "Priority",
                    visible: true
                },
                {
                    name: "description",
                    label: "Description",
                    visible: true,
                    customRender: (value) => {
                        if (typeof window !== "undefined" && this.ReactQuill) {
                            const ReactQuill = this.ReactQuill;
                            return (
                                <Fragment>
                                    <ReactQuill
                                        readOnly={true}
                                        value={value}
                                        modules={{ toolbar: false }}
                                        theme={null}
                                        style={{
                                            padding: 0
                                        }}
                                    />
                                </Fragment>
                            )
                        }
                    }
                },
                {
                    name: "touchPoint",
                    label: "Related",
                    visible: true,
                    customRender: (value) => {
                        if (value) {
                            return (
                                <Link href={`/touchPoint?id=${value.id}`}>
                                    <a>TouchPoint</a>
                                </Link>
                            )
                        }
                    }
                },
                {
                    name: "complete",
                    label: "Complete",
                    visible: true,
                    customRender: (value, record) => {
                        return (
                            <Checkbox slider checked={value} fitted onChange={() => this.toggleComplete(record.id)} />
                        )
                    }
                },
                {
                    name: "id",
                    label: "Controls",
                    visible: true,
                    textAlign: 'center',
                    customRender: (value) => {
                        let stateName = `confirmOpen-${value}`
                        return (
                            <div>
                                <Link href={`/actionItem?id=${value}`}>
                                    <Icon link name='edit outline' />
                                </Link>
                                <Icon link color='red' name='trash alternate outline' onClick={() => this.showConfirm(stateName)} />
                                <Confirm
                                    open={this.state[stateName]}
                                    onCancel={() => this.handleCancel(stateName)}
                                    onConfirm={() => this.onRowDelete(value)}
                                    content={`Are you sure you wish to delete ${value}?`} />
                            </div>
                        );
                    }
                }
            ],
        })
    }

    showConfirm(name) {
        this.setState({ [name]: true })
    }

    toggleComplete = (id) => {
        //TODO: Implement this along with automatic saving
        console.log(id)
        let thisAction = this.props.actionItemStore.find(id)
        thisAction.toggleComplete()
        this.props.actionItemStore.save(thisAction)
        //this.setState(prevState => ({ checked: !prevState.checked }))
    }

    handleCancel = (name) => this.setState({ [name]: false })

    onRowDelete(rowToDelete) {
        console.log(`Deleting ${JSON.stringify(rowToDelete)}`)
        try {
            this.props.actionItemStore.deleteActionItem(rowToDelete)
            this.setState({
                [`confirmOpen-${rowToDelete}`]: false
            })
        } catch (error) {

        }
        console.log(rowToDelete, "were deleted!");
    }

    headerControls = (saveMethod) => {
        return (
            <ModalActionItemForm
                open={this.props.newTouchPointOpen}
                saveMethod={saveMethod}
                trigger={
                    <Button compact color='green' basic icon='add' />
                } />
        )
    }

    render() {
        return (
            <SortableTable attached
                title={this.props.title ? this.props.title : 'Action Items'}
                data={this.props.actionItems}
                columns={this.state.actionItemColumns}
                headerControls={this.headerControls(this.props.saveMethod)}
            />
        )
    }
}

export default ActionItemsList;