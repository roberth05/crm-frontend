import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'semantic-ui-react';

class CompanyInfoCard extends React.Component {
    render() {
        const { company } = this.props;
        return (
            <div id={`company-${company.id}`}>
                <Card>
                    <Card.Content>
                        <Card.Header>{company.name}</Card.Header>
                        <Card.Meta>
                            <a href={company.website} target="_blank">{company.website}</a>
                        </Card.Meta>
                        <Card.Description>
                            TODO: Add a comments field.
                        </Card.Description>
                    </Card.Content>
                </Card>
            </div>
        )
    }
}

CompanyInfoCard.propTypes = {
    company: PropTypes.object.isRequired
};

export default CompanyInfoCard;