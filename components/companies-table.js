import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import SortableTable from './sortable-table';
import { observer } from 'mobx-react';

@observer
export default class CompaniesTable extends React.Component {

    static propTypes = {
        companies: PropTypes.array.isRequired,
    };

    componentWillMount() {
        //console.log(`peopleTable : ${JSON.stringify(this.props.people)}`)
        this.setState({
            //companies: this.props.companies,
            columns: [
                {
                    name: "name",
                    label: "Name",
                    visible: true
                },
                {
                    name: "website",
                    label: "Website",
                    visible: true,
                    customRender: (value) => {
                        return (<a href={value} target="_blank">{value}</a>)
                    }
                },
                {
                    name: "id",
                    label: "Edit",
                    visible: true,
                    customRender: (value) => {
                        return (
                            <Link href={`/company?id=${value}`}>
                                <a>Edit</a>
                            </Link>
                        );
                    }
                },
            ]
        })
    }

    render() {
        return (
            <Fragment>
                <SortableTable data={this.props.companies} columns={this.state.columns} />
            </Fragment>
        )
    }
}