import React, { Fragment } from 'react';
import Link from 'next/link';
import SortableTable from './sortable-table';
import { formatDateTime } from '../src/utils';
import { Icon, Confirm, Button } from 'semantic-ui-react';
import { ModalTouchPointForm } from './forms/modal-touchpoint-form';
import { observer, inject } from 'mobx-react';
// import ReactQuill from 'react-quill';

@inject('touchPointStore')
@observer
class TouchPointsList extends React.Component {
    constructor(props) {
        super(props)
        if (typeof window !== "undefined") {
            this.ReactQuill = require("react-quill");
        }
    }

    ReactQuill = null;

    componentWillMount() {
        //console.log(`componentWillMount showPerson: ${this.props.showPerson}`)
        this.setState({
            title: this.props.title || "Touch Points",
            showPerson: this.props.showPerson,
            confirmOpen: {},
            person: this.props.person,
            touchpointColumns: [
                {
                    name: "id",
                    label: "Id",
                    visible: false
                },
                {
                    name: "date",
                    label: "Date",
                    visible: true,
                    customRender: (value) => {
                        return formatDateTime(value)
                    }
                },
                {
                    name: "person",
                    label: "Person",
                    visible: this.props.showPerson,
                    customRender: (value) => {
                        if (this.props.showPerson && value) {
                            return (
                                <Link href={`/person?id=${value.id}`}>
                                    <a>{`${value.firstName} ${value.lastName}`}</a>
                                </Link>
                            )
                        } else {
                            return (
                                <div></div>
                            )
                        }
                    }
                },
                {
                    name: "method",
                    label: "Method",
                    visible: true
                },
                {
                    name: "discussed",
                    label: "Discussed",
                    visible: true,
                    customRender: (value) => {
                        if (typeof window !== "undefined" && this.ReactQuill) {
                            const ReactQuill = this.ReactQuill;
                            return (
                                <Fragment>
                                    <ReactQuill
                                        readOnly={true}
                                        value={value}
                                        modules={{ toolbar: false }}
                                        theme={null}
                                        style={{
                                            padding: 0
                                        }}
                                    />
                                </Fragment>
                            )
                        }
                    }
                },
                {
                    name: "initiatedByMe",
                    label: "Initiated By Me",
                    visible: true,
                    customRender: (value) => {
                        return (value ? "Yes" : "No")
                    }
                },
                {
                    name: "id",
                    label: "Controls",
                    visible: true,
                    textAlign: 'center',
                    customRender: (value) => {
                        // console.log(value)
                        let stateName = `confirmOpen-${value}`
                        //this.state[stateName] = false;
                        return (
                            <div>
                                <Link href={`/touchpoint?id=${value}`}>
                                    <Icon link name='edit outline' />
                                </Link>
                                <Icon link color='red' name='trash alternate outline' onClick={() => this.showConfirm(stateName)} />
                                <Confirm
                                    open={this.state[stateName]}
                                    onCancel={() => this.handleCancel(stateName)}
                                    onConfirm={() => this.onRowDelete(value)}
                                    content={`Are you sure you wish to delete ${value}?`} />
                            </div>
                        );
                    }
                }
            ],
        })
    }

    showConfirm(name) {
        // console.log(`showConfirm: ${name}`)
        this.setState({ [name]: true })
    }

    handleCancel = (name) => this.setState({ [name]: false })

    onRowDelete(rowToDelete) {
        console.log(`Deleting ${JSON.stringify(rowToDelete)}`)
        try {
            this.props.touchPointStore.deleteTouchPoint(rowToDelete)
            this.setState({
                // touchpoints: _.remove(this.touchpoints, function (tp) {
                //     return tp.id === rowToDelete
                // }),
                [`confirmOpen-${rowToDelete}`]: false
            })
        } catch (error) {

        }
        console.log(rowToDelete, "were deleted!");
    }

    headerControls = (saveMethod) => {
        return (
            <ModalTouchPointForm
                open={this.props.newTouchPointOpen}
                saveMethod={saveMethod}
                trigger={
                    <Button compact color='green' basic icon='add' onClick={this.handleTouchPointOpen} />
                } />
        )
    }

    render() {
        return (
            <SortableTable attached
                title={this.props.title ? this.props.title : 'Recent Touch Points'}
                data={this.props.touchpoints}
                columns={this.state.touchpointColumns}
                headerControls={this.headerControls(this.props.saveMethod)}
            />
        )
    }
}

export default TouchPointsList;