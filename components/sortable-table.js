import _ from 'lodash'
import React, { Component } from 'react'
import { Table, Header } from 'semantic-ui-react'
import { observer } from 'mobx-react';

@observer
export default class SortableTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sortedColumn: props.column,
            columns: props.columns,
            data: props.data,
            direction: props.direction,
            attached: props.attached
        }
    }

    static getDerivedStateFromProps(props, state) {
        let data = props.data;
        return { data: _.orderBy(data, state.sortedColumn, [state.direction]) }
    }

    handleSort = clickedColumn => () => {
        const { sortedColumn, data, direction } = this.state
        if (sortedColumn !== clickedColumn) {
            this.setState({
                sortedColumn: clickedColumn,
                data: _.orderBy(data, [clickedColumn]),
                direction: 'asc',
            })
            return
        }

        this.setState({
            direction: direction === 'asc' ? 'desc' : 'asc',
            data: _.orderBy(data, [clickedColumn], [direction])
        })
    }

    getHeaderCell(name, label, visible) {
        if (visible) {
            return (
                <Table.HeaderCell
                    key={name}
                    sorted={this.state.sortedColumn === { name } ? direction : null}
                    onClick={this.handleSort(name)}
                >
                    {label}
                </Table.HeaderCell>
            )
        }
    }

    getBodyRow(record, columns) {
        return (
            <Table.Row key={record.id}>
                {_.map(columns, ({ name, label, customRender, visible, textAlign }) => (
                    this.getBodyCell(name, label, visible, customRender, record, textAlign)
                )
                )}
            </Table.Row>
        )
    }

    getBodyCell(name, label, visible, customRender, record, textAlign) {
        //console.log(JSON.stringify(record))
        if (visible) {
            //console.log(`getBodyCell: ${JSON.stringify(record)} ${name}`)
            return (
                <Table.Cell key={`${record.id}-${name}`} textAlign={textAlign ? textAlign : 'left'}>{customRender ? customRender(record[name], record) : record[name]}</Table.Cell>
            )
        }
    }

    countVisibleColumns() {
        return _.countBy(this.state.columns, 'visible')['true']
    }

    render() {
        const { sortedColumn, data, direction } = this.state
        return (
            <Table sortable attached={this.props.attached ? true : false} compact stackable>
                <Table.Header fullWidth>
                    {this.props.title &&
                        <Table.Row>
                            <Table.HeaderCell colSpan={this.countVisibleColumns() - 1}>
                                <Header size='medium'>{this.props.title}</Header>
                            </Table.HeaderCell>
                            <Table.HeaderCell colSpan={1} textAlign='center'>
                                {this.props.headerControls}
                            </Table.HeaderCell>
                        </Table.Row>
                    }
                    <Table.Row key="columnHeaders">
                        {_.map(this.state.columns, ({ name, label, visible }) => (
                            this.getHeaderCell(name, label, visible)
                        )
                        )}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {_.map(data, (record) => (
                        this.getBodyRow(record, this.state.columns)
                    ))}
                </Table.Body>
            </Table>
        )
    }
}