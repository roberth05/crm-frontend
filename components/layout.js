import React from 'react';
import PropTypes from 'prop-types';
import ApplicationHeader from './header';
import { observer, inject } from 'mobx-react';

@inject('uiStore')
@observer
class Layout extends React.Component {

  render() {
    const { children, title } = this.props;

    return (
      <div>
        <ApplicationHeader
          title={title}
          isAuthenticated={this.props.isAuthenticated}
          loggedUser={this.props.loggedUser}
          uiStore={this.props.uiStore}
        >
          {children}
        </ApplicationHeader>
      </div>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.object.isRequired
};

export default Layout;