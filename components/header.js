import React from 'react';
import Link from 'next/link';
import { logout } from '../src/auth';

import { Sidebar, Menu, Segment, Icon, Container, Button, Dropdown } from 'semantic-ui-react';
import { ModalPersonForm } from './modal-person-form';
import { ModalTouchPointForm } from './forms/modal-touchpoint-form';

import { observer, inject } from 'mobx-react';
import { ModalActionItemForm } from './forms/modal-actionItem-form';

const primaryLinks = [
  {
    "Text": "Home",
    "href": "/",
    "icon": "home"
  },
  {
    "Text": "People",
    "href": "/people",
    "icon": "address card outline"
  },
  {
    "Text": "Companies",
    "href": "/companies",
    "icon": "building outline"
  }];

const secondaryLinks = [
  {
    "Text": "About",
    "href": "/about"
  }
];

@inject('uiStore', 'personGroupStore', 'peopleStore', 'touchPointStore')
@observer
class ApplicationHeader extends React.Component {
  state = {
    open: false,
    newPersonOpen: false,
    newTouchPointOpen: false
  };

  //handleToggle = () => this.setState({ open: !this.state.open })

  handleToggle = () => {
    console.log(`handeToggle: ${this.props.uiStore.sidebarOpen}`)
    //this.props.uiStore.setUISetting('sidebarOpen', !this.props.uiStore.uiSettings.get('sidebarOpen'))
    this.props.uiStore.sidebarOpen = !this.props.uiStore.sidebarOpen
  }

  handlePersonOpen = () => { this.props.uiStore.newPersonOpen = true }

  handleTouchPointOpen = () => { this.props.uiStore.newTouchPointOpen = true }

  handleActionItemOpen = () => { this.props.uiStore.newActionItemOpen = true }

  addNewPersonOnSave = async (data) => {
    //console.log(`addPersonOnSave: ${JSON.stringify(data)}`)
    try {
      const response = await this.props.peopleStore.saveNew(data);
      this.props.uiStore.newPersonOpen = false;
    } catch (err) {
      throw err
    }
  };

  addNewTouchPointOnSave = async (data) => {
    try {
      let newTP = await this.props.touchPointStore.saveNew(data);
      if (data.actionItems.length != 0) {
        data.actionItems.forEach(actionItem => {
          actionItem.setTouchPoint(newTP)
          this.props.actionItemStore.saveNew(actionItem)
        });
      }

      this.props.uiStore.newTouchPointOpen = false;
    } catch (err) {
      throw err
    }
  };

  addNewActionItemOnSave = async (data) => {
    try {
      this.props.actionItemStore.saveNew(data);

      this.props.uiStore.newActionItemOpen = false;
    } catch (err) {
      throw err
    }
  };

  render() {
    const { title, isAuthenticated, children, loggedUser, uiStore } = this.props;
    // console.log(this.props)
    const { open } = this.state;

    const sidebarOpen = uiStore.sidebarOpen;

    // console.log(`Render sidebar: ${sidebarOpen}`)

    return (
      <div style={{ height: '100vh' }}>
        <Sidebar.Pushable>
          <Sidebar
            as={Menu}
            animation='slide along'
            direction='left'
            icon='labeled'
            vertical
            visible={sidebarOpen}
            width='thin'
          >
            {primaryLinks.map((obj, index) => (
              <Link href={`${obj.href}`} key={index}>
                <Menu.Item as='a'>
                  {obj.icon &&
                    <Icon name={obj.icon} />
                  }
                  {obj.Text}
                </Menu.Item>
              </Link>
            ))
            }
            {secondaryLinks.map((obj, index) => (
              <Link href={`${obj.href}`} key={index}>
                <Menu.Item as='a'>
                  {obj.icon &&
                    <Icon name={obj.icon} />
                  }
                  {obj.Text}
                </Menu.Item>
              </Link>
            ))
            }
          </Sidebar>
          <Sidebar.Pusher>
            <Segment
              style={{ minHeight: 48, padding: '1em 0em' }}>
              <Container>
                <Menu borderless fixed='top' size='large'>
                  <Menu.Item onClick={this.handleToggle}>
                    <Icon name='sidebar' />
                  </Menu.Item>
                  <Menu.Item fitted>
                    <Dropdown icon='add'>
                      <Dropdown.Menu>
                        <Dropdown.Item>
                          <ModalTouchPointForm
                            open={this.props.uiStore.newTouchPointOpen}
                            saveMethod={this.addNewTouchPointOnSave}
                            peopleList={this.props.peopleStore.people}
                            trigger={
                              <a onClick={this.handleTouchPointOpen}>Touch Point</a>
                            } />
                        </Dropdown.Item>
                        <Dropdown.Item>
                          <ModalActionItemForm
                            open={this.props.uiStore.newTouchPointOpen}
                            saveMethod={this.addNewActionItemOnSave}
                            trigger={
                              <a onClick={this.handleActionItemOpen}>Action Item</a>
                            } />
                        </Dropdown.Item>
                        <Dropdown.Item>
                          <ModalPersonForm
                            open={this.props.newPersonOpen}
                            saveMethod={this.addNewPersonOnSave}
                            trigger={
                              <a onClick={this.handlePersonOpen}>Person</a>
                            } />
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </Menu.Item>
                  <Menu.Item header as="h3">{title}</Menu.Item>
                  <Menu.Item header as="h4" position='right' fitted>{loggedUser}</Menu.Item>
                  <Menu.Item>
                    <Button as='a' basic onClick={logout}>
                      Logout
                    </Button>
                  </Menu.Item>
                </Menu>
              </Container>
            </Segment>
            <div className="main-content" style={{ margin: '1.5em' }}>{children}</div>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div >
    )
  }
}

export default ApplicationHeader;