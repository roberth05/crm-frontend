import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import SortableTable from './sortable-table';
import { formatDate } from '../src/utils';
import { Icon } from 'semantic-ui-react';
import { inject, observer } from 'mobx-react';

@inject('uiStore', 'peopleStore')
@observer
export default class PeopleToContactTable extends React.Component {

    static propTypes = {
        people: PropTypes.array.isRequired,
    };

    componentWillMount() {
        this.setState({
            confirmOpen: false,
            newPersonOpen: false,
            columns: [
                {
                    name: "firstName",
                    label: "First Name",
                    visible: true
                },
                {
                    name: "lastName",
                    label: "Last Name",
                    visible: true
                },
                {
                    name: "latestTouchPoint",
                    label: "Latest Touch Point",
                    visible: true,
                    customRender: (value) => {
                        if (value) return (formatDate(value.date))
                    }
                },
                {
                    name: "nextTouchPointBy",
                    label: "Next Touch Point",
                    visible: true,
                    customRender: (value) => {
                        if (value) {
                            if (value < new Date()) {
                                return (
                                    <Fragment>
                                        {formatDate(value)}
                                        <Icon name="exclamation" color="red" />
                                    </Fragment>
                                )
                            }
                            return (formatDate(value))
                        }
                    }
                },
                {
                    name: "id",
                    label: "Controls",
                    visible: true,
                    textAlign: "center",
                    customRender: (value) => {
                        return (
                            <Fragment>
                                <Link href={`/person?id=${value}`}>
                                    <Icon link name='edit outline' />
                                </Link>
                            </Fragment>
                        );
                    }
                },
            ]
        })
    }

    render() {
        return (
            <Fragment>
                <SortableTable
                    title="People To Contact"
                    data={this.props.people}
                    columns={this.state.columns}
                />
            </Fragment>
        )
    }
}