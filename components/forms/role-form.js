import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import _ from 'lodash'

import { Form, Header, Button } from 'semantic-ui-react';

var parseDate = require('date-fns/parse');

class RoleForm extends React.Component {
    static propTypes = {
        onSave: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            peopleOptions:
                _.map(this.props.peopleList, person => ({
                    key: person.id,
                    text: `${person.firstName} ${person.lastName}`,
                    value: person.id
                })),
            companyOptions:
                _.map(this.props.companiesList, company => ({
                    key: company.id,
                    text: `${company.name}`,
                    value: company.id
                })),
        };
    }

    onSubmit = (event) => {
        event.preventDefault();
        //console.log(`onSubmit person: ${JSON.stringify(this.state.person)}`)

        this.props.onSave(this.props.role);
    };

    onDateChange = name => event => {
        console.log(event)
        console.log(`onDateChange: ${event}, ${name}`)
        this.setState({
            role: Object.assign(this.props.role, { [name]: event })
        })
        console.log(this.props.role)
    };

    handleChange = (e, { name, value }) => this.setState({
        role: Object.assign(this.props.role, { [name]: value })
    })

    handleSelectChange = (e, { name, value }) => {
        //console.log(`handleGroupChange: ${name} ${value}`)
        var propsName = '';

        switch (name) {
            case 'company':
                propsName = 'companiesList'
                break;
            case 'person':
                propsName = 'peopleList'
                break;
            default:
                break;
        }

        var selected = _.find(this.props[propsName], item => {
            console.log(`handleSelectChange: ${name} ${propsName} ${value} `)
            return item.id === value
        })

        console.log(`Selected: ${JSON.stringify(selected)}`)
        this.setState({
            role: Object.assign(this.props.role, { [name]: selected })
        })
        console.log(JSON.stringify(this.props.role))
    }

    render() {
        return (
            <div style={{ margin: '1.5em' }}>
                <Header as="h3">{this.props.title ? this.props.title : "Add New Role"}</Header>
                <Form size='large' onSubmit={this.onSubmit}>
                    <Form.Input
                        placeholder='Title'
                        value={this.props.role.title}
                        name='title'
                        onChange={this.handleChange}
                        required
                    />
                    <DatePicker
                        selected={this.props.role.started ? parseDate(this.props.role.started) : null}
                        onChange={this.onDateChange('started')}
                        name="date"
                        todayButton="Today"
                        placeholderText="Started"
                    />
                    <DatePicker
                        selected={this.props.role.ended ? parseDate(this.props.role.ended) : null}
                        onChange={this.onDateChange('ended')}
                        name="date"
                        todayButton="Today"
                        placeholderText="ended"
                    />
                    <br /><br />
                    {this.props.showPerson &&
                        <Form.Select
                            name='person'
                            search
                            searchInput={{ id: 'form-select-person' }}
                            options={this.state.peopleOptions}
                            onChange={this.handleSelectChange}
                            // value={this.state.role.person ? this.state.role.person.id : null}
                        />
                    }
                    {this.props.showCompany &&
                        <Form.Select
                            name='company'
                            search
                            searchInput={{ id: 'form-select-company' }}
                            options={this.state.companyOptions}
                            onChange={this.handleSelectChange}
                            // value={this.state.role.company ? this.state.role.company.id : null}
                        />
                    }
                    <Button variant="contained" type="submit" fluid>
                        Save
                    </Button>
                </Form>
            </div >
        );
    }
}

export default RoleForm;