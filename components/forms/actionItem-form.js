import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import DatePicker from "react-datepicker";
import { priorities } from '../../src/model/actionItem';

// import ReactQuill from 'react-quill';

import "react-datepicker/dist/react-datepicker.css";

import _ from 'lodash';

import { Header, Button, Form } from 'semantic-ui-react';

class ActionItemForm extends React.Component {
    static propTypes = {
        onSave: PropTypes.func.isRequired,
    };

    static defaultProps = {
        actionItem: null,
    };

    ReactQuill = null;

    constructor(props) {
        super(props);
        if (typeof window !== "undefined") {
            this.ReactQuill = require("react-quill");
        }

        this.state = {
            priorities:
                _.map(["High", "Normal", "Low"], method => ({
                    key: method,
                    text: method,
                    value: priorities[method.toUpperCase()]
                })),
            descriptionValue: ''
        };
    }

    onSubmit = (event) => {
        event.preventDefault();

        this.props.onSave(this.props.actionItem);
    };

    handleDescriptionChanged = (value) => {
        this.setState({ descriptionValue: value })
        this.props.actionItem.description = value
    }

    handleChange = (e, { name, value }) => {
        this.setState({
            actionItem: Object.assign(this.props.actionItem, { [name]: value })
        })
    }

    onDateChange = name => value => {
        this.setState({
            actionItem: Object.assign(this.props.actionItem, { due: value })
        })
    };

    modules = {
        toolbar: false
    }

    editorComponent() {
        if (typeof window !== "undefined" && this.ReactQuill) {
            const ReactQuill = this.ReactQuill;
            return (
                <Fragment>
                    <ReactQuill value={this.state.descriptionValue}
                        onChange={this.handleDescriptionChanged}
                        theme="bubble"
                        style={{
                            borderRadius: "0.28571429rem",
                            border: "1px solid rgba(34,36,38,.15)",
                            color: "rgba(0,0,0,.87)"
                        }}
                        placeholder="Discussed..."
                    />
                    <br />
                </Fragment>
            )
        } else {
            return ''
        }
    }

    render() {
        return (
            <div >
                <Header as="h3">{this.props.title ? this.props.title : "Add New Action Item"}</Header>
                <Form size='large' onSubmit={this.onSubmit}>
                    <DatePicker
                        selected={this.props.actionItem.due}
                        onChange={this.onDateChange('due')}
                        showTimeSelect
                        dateFormat="Pp"
                        name="due"
                        todayButton="Today"
                        placeholderText="Due Date"
                    />
                    <br /><br />
                    {this.editorComponent()}
                    <Form.Select
                        options={this.state.priorities}
                        placeholder='Priority'
                        name='priority'
                        search
                        searchInput={{ id: 'form-select-control-priority' }}
                        onChange={this.handleChange}
                        value={this.props.actionItem.priority}
                    />
                    <Button fluid>
                        Save
                    </Button>
                </Form>
            </div>
        );
    }
}

export default ActionItemForm;