import { Modal, Header, Button, Icon } from "semantic-ui-react";
import { observer, inject } from "mobx-react";
import PropTypes from 'prop-types';
import RoleForm from "./role-form";
import PersonRole from '../../src/model/person-role';

@inject('uiStore')
@observer
export class ModalRoleForm extends React.Component {
    static propTypes = {
        saveMethod: PropTypes.func.isRequired,
    };

    state = {
        role: null,
        open: false
    }

    constructor(props) {
        super(props)
        this.state = {
            role: props.role ? props.role : new PersonRole(null, null, null, null, props.person, props.company),
            open: props.open
        }
    }

    handleFormCancel = () => {
        console.log(`Closing form`)
        this.props.uiStore.newRoleOpen = false;
    }

    handleFormOpen = () => { this.props.uiStore.newRoleOpen = true }

    render() {
        return (
            <Modal
                open={this.props.uiStore.newRoleOpen}
                onClose={this.handleFormCancel}
                onOpen={this.handleFormOpen}
                size='small'
                trigger={this.props.trigger}
            >
                <Header icon='browser' content={this.props.title ? this.props.title : 'Add New Role'} />
                <Modal.Content>
                    <RoleForm
                        onSave={this.props.saveMethod}
                        peopleList={this.props.peopleList}
                        companiesList={this.props.companiesList}
                        showPerson={this.props.person ? false : true}
                        showCompany={this.props.company ? false : true}
                        role={this.state.role}
                    />
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red' onClick={this.handleFormCancel}>
                        <Icon name='cancel' />Cancel
                </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}