import ActionItem from '../../src/model/actionItem';
import { Modal, Header, Button, Icon } from "semantic-ui-react";
import ActionItemForm from "./actionItem-form";
import { inject, observer } from "mobx-react";

@inject('uiStore')
@observer
export class ModalActionItemForm extends React.Component {
    state = {
        open: false,
    }

    constructor(props) {
        super(props)
        this.state = {
            open: props.open,
            actionItem: props.actionItem ? props.actionItem : new ActionItem(null, null, 50, new Date(), false)
        }
    }

    handleCancel = () => {
        console.log(`Closing form`)
        this.props.uiStore.newActionItemOpen = false;
    }

    handleOpen = () => this.props.uiStore.newActionItemOpen = true;

    render() {
        return (
            <Modal
                open={this.props.uiStore.newActionItemOpen}
                onClose={this.handleCancel}
                onOpen={this.handleOpen}
                size='small'
                trigger={this.props.trigger}
            >
                <Header icon='browser' content={this.props.title ? this.props.title : 'Add New Action Item'} />
                <Modal.Content>
                    <ActionItemForm
                        onSave={this.props.saveMethod}
                        actionItem={this.state.actionItem}
                    />
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red' onClick={this.handleCancel}>
                        <Icon name='cancel' />Cancel
                </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}