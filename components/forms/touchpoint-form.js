import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import DatePicker from "react-datepicker";
import { priorities } from '../../src/model/actionItem';

import "react-datepicker/dist/react-datepicker.css";

import _ from 'lodash';

import { Header, Button, Form, Grid, GridColumn, GridRow } from 'semantic-ui-react';
import ActionItem from '../../src/model/actionItem';

class TouchPointForm extends React.Component {
    static propTypes = {
        onSave: PropTypes.func.isRequired,
    };

    static defaultProps = {
        touchPoint: null,
    };

    ReactQuill = null;

    constructor(props) {
        super(props);
        if (typeof window !== "undefined") {
            this.ReactQuill = require("react-quill");
        }

        this.state = {
            person: props.person,
            methods:
                _.map(["In-person", "Phone call", "Meeting", "Drop-In/Standup", "Other"], method => ({
                    key: method,
                    text: method,
                    value: method
                })),
            priorities:
                _.map(["High", "Normal", "Low"], method => ({
                    key: method,
                    text: method,
                    value: priorities[method.toUpperCase()]
                })),
            peopleList:
                _.map(this.props.people, person => ({
                    key: person.id,
                    text: `${person.firstName} ${person.lastName}`,
                    value: person.id
                })),
            people: props.people,
            discussedValue: '',
            actionItem: null
        };
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.props.onSave(this.props.touchPoint);
    };

    handleDiscussedChange = (value) => {
        this.setState({ discussedValue: value })
        this.props.touchPoint.discussed = value
    }

    handleChange = (e, { name, value }) => {
        //console.log(`HandleChange: ${name} ${value}`)
        if (name == 'person') {
            const selectedPerson = _.find(this.state.people, function (person) {
                return person.id === value;
            })
            this.setState({
                touchPoint: Object.assign(this.props.touchPoint, {
                    [name]: selectedPerson
                }),
                person: selectedPerson
            })
        } else {
            this.setState({
                touchPoint: Object.assign(this.props.touchPoint, { [name]: value })
            })
        }
    }

    handlePriorityChange = (e, { value }) => {
        if (this.state.actionItem) {
            this.setState({
                actionItem: Object.assign(this.state.actionItem, { priority: value })
            })
        }
    }

    onDateChange = name => value => {
        console.log(`onDateChange: ${value}, ${name}`)
        this.setState({
            touchPoint: Object.assign(this.props.touchPoint, { date: value })
        })
    };

    onDueChange = name => value => {
        console.log(value)
        this.setState({
            actionItem: Object.assign(this.state.actionItem, { due: value })
        })
    };

    changeInitiatedByMe = (e, { value }) => {
        this.setState({
            touchPoint: Object.assign(this.props.touchPoint, { initiatedByMe: !this.props.touchPoint.initiatedByMe })
        })
    }

    changeRequiresAction = (e, data) => {
        const value = data.checked;
        if (value) {
            if (!this.state.actionItem) {
                const newAction = new ActionItem('unsavedActionItem', `Action required for Touch Point`, 50, null, false, this.props.touchPoint);
                this.setState({
                    actionItem: newAction
                })
                this.props.touchPoint.addActionItem(newAction)
            }
        }
    }

    modules = {
        toolbar: false
    }

    editorComponent() {
        if (typeof window !== "undefined" && this.ReactQuill) {
            const ReactQuill = this.ReactQuill;
            return (
                <Fragment>
                    <ReactQuill value={this.state.discussedValue}
                        onChange={this.handleDiscussedChange}
                        // modules={this.modules}
                        theme="bubble"
                        style={{
                            borderRadius: "0.28571429rem",
                            border: "1px solid rgba(34,36,38,.15)",
                            color: "rgba(0,0,0,.87)"
                        }}
                        placeholder="Discussed..."
                    />
                    <br />
                </Fragment>
            )
        } else {
            return ''
        }
    }

    render() {
        return (
            <div >
                <Header as="h3">{this.props.title ? this.props.title : "Add New Touch Point"}</Header>
                <Form size='large' onSubmit={this.onSubmit}>
                    <DatePicker
                        selected={this.props.touchPoint.date}
                        onChange={this.onDateChange('date')}
                        showTimeSelect
                        dateFormat="Pp"
                        name="date"
                        todayButton="Today"
                        placeholderText="Date Occurred"
                    />
                    <br /><br />
                    {this.props.showPerson &&
                        <Form.Select
                            options={this.state.peopleList}
                            placeholder='Person'
                            name='person'
                            search
                            searchInput={{ id: 'form-select-control-person' }}
                            onChange={this.handleChange}
                        />
                    }
                    {this.editorComponent()}
                    <Form.Select
                        options={this.state.methods}
                        placeholder='Method'
                        name='method'
                        search
                        searchInput={{ id: 'form-select-control-gender' }}
                        onChange={this.handleChange}
                        value={this.props.touchPoint.method}
                    />
                    <Form.Checkbox
                        toggle
                        label="Initiated By Me"
                        onChange={this.changeInitiatedByMe} checked={this.props.touchPoint.initiatedByMe}
                    />
                    <Grid stretched>
                        <GridRow verticalAlign='middle'>
                            <GridColumn width='5'>
                                <Form.Checkbox
                                    toggle
                                    label="Requires Action"
                                    onChange={this.changeRequiresAction} checked={(this.props.touchPoint.actionItems.length != 0)}
                                />
                            </GridColumn>
                            <GridColumn width='6'>
                                {(this.props.touchPoint.actionItems.length != 0) &&
                                    <DatePicker
                                        selected={this.state.actionItem.due}
                                        onChange={this.onDueChange('due')}
                                        showTimeSelect
                                        dateFormat="Pp"
                                        name="due"
                                        todayButton="Today"
                                        placeholderText="Due Date"
                                    />
                                }
                            </GridColumn>
                            <GridColumn width='5'>
                                {(this.props.touchPoint.actionItems.length != 0) &&
                                    <Form.Select
                                        options={this.state.priorities}
                                        placeholder='Priority'
                                        name='priority'
                                        onChange={this.handlePriorityChange}
                                        value={this.state.actionItem.priority}
                                        fluid
                                    />
                                }
                            </GridColumn>
                        </GridRow>
                    </Grid>
                    <br />
                    <Button fluid>
                        Save
                    </Button>
                </Form>
            </div >
        );
    }
}

export default TouchPointForm;