import TouchPoint from '../../src/model/touchpoint';
import { Modal, Header, Button, Icon } from "semantic-ui-react";
import TouchPointForm from "./touchpoint-form";
import { inject, observer } from "mobx-react";

@inject('uiStore', 'peopleStore')
@observer
export class ModalTouchPointForm extends React.Component {
    state = {
        peopleList: [],
        person: null,
        open: false,
    }

    constructor(props) {
        super(props)
        this.state = {
            peopleList: props.peopleList,
            person: props.person,
            open: props.open,
            touchPoint: props.touchPoint ? props.touchPoint : new TouchPoint(null, null, null, new Date(), null, props.person)
        }
    }

    handleCancel = () => {
        console.log(`Closing form`)
        this.setState({ touchPoint: null })
        this.props.uiStore.newTouchPointOpen = false;
    }

    handleOpen = () => {
        if(!this.state.touchPoint) this.setState({ touchPoint: new TouchPoint(null, null, null, new Date(), null, this.props.person) })
        this.props.uiStore.newTouchPointOpen = true;
    }

    render() {
        return (
            <Modal
                open={this.props.uiStore.newTouchPointOpen}
                onClose={this.handleCancel}
                onOpen={this.handleOpen}
                size='small'
                trigger={this.props.trigger}
            >
                <Header icon='browser' content={this.props.title ? this.props.title : 'Add New Touch Point'} />
                <Modal.Content>
                    <TouchPointForm
                        onSave={this.props.saveMethod}
                        people={this.props.peopleStore.people}
                        person={this.props.uiStore.selectedPerson}
                        showPerson={this.props.uiStore.selectedPerson ? false : true}
                        touchPoint={this.state.touchPoint}
                    />
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red' onClick={this.handleCancel}>
                        <Icon name='cancel' />Cancel
                </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}