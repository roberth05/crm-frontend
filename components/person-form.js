import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import _ from 'lodash'

import { Form, Header, Button, Dimmer, Loader } from 'semantic-ui-react';
import Person from '../src/model/person';
import { inject } from 'mobx-react';

var parseDate = require('date-fns/parse');

@inject('personGroupStore')
class PersonForm extends React.Component {
    static propTypes = {
        onSave: PropTypes.func.isRequired,
    };

    static defaultProps = {
        person: null,
    };

    mapGroupsToOptions(groups) {
        return _.map(groups, group => ({
            key: group.id,
            text: group.name,
            value: group.id
        }))
    }
    constructor(props) {
        super(props);

        this.state = {
            person: props.person ? props.person : new Person(),
            loading: false
        };
    }

    componentDidMount() {
        if (!this.state.groups) {
            this.props.personGroupStore.loadPersonGroups().then(() => {
                this.setState({
                    groupOptions: this.mapGroupsToOptions(this.props.personGroupStore.personGroups),
                    groups: this.props.personGroupStore.personGroups
                })
            })
        }
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.setState({
            loading: true
        })
        //console.log(`onSubmit person: ${JSON.stringify(this.state.person)}`)

        this.props.onSave(this.state.person);
    };

    onDateChange = name => event => {
        console.log(event)
        console.log(`onDateChange: ${event}, ${name}`)
        this.setState({
            person: Object.assign({}, this.state.person, { [name]: parseDate(event) })
        })
    };

    handleChange = (e, { name, value }) => this.setState({
        person: Object.assign(this.state.person, { [name]: value })
    })

    handleGroupChange = (e, { name, value }) => {
        //console.log(`handleGroupChange: ${name} ${value}`)
        var groups = _.filter(this.state.groups, group => {
            console.log(`handleGroupChange group: ${value}`)
            return _.includes(value, group.id)
        })
        //console.log(JSON.stringify(groups))
        this.setState({
            person: Object.assign(this.state.person, { groups: groups })
        })
        //console.log(JSON.stringify(this.state.person))
    }

    render() {
        return (
            <div style={{ margin: '1.5em' }}>
                <Dimmer active={this.state.loading}>
                    <Loader>Saving</Loader>
                </Dimmer>
                <Header as="h3">{this.props.title ? this.props.title : "Add New Person"}</Header>
                <Form size='large' onSubmit={this.onSubmit}>
                    <Form.Input
                        placeholder='First Name'
                        value={this.state.person.firstName}
                        name='firstName'
                        onChange={this.handleChange}
                        required
                    />
                    <Form.Input
                        placeholder='Last Name'
                        value={this.state.person.lastName}
                        name='lastName'
                        onChange={this.handleChange}
                        required
                    />
                    <DatePicker
                        selected={this.state.person.birthday ? parseDate(this.state.person.birthday) : null}
                        onChange={this.onDateChange('birthday')}
                        name="date"
                        todayButton="Today"
                        placeholderText="Birthday"
                    />
                    <br /><br />
                    <Form.Select
                        multiple
                        name='persongroups'
                        search
                        searchInput={{ id: 'form-select-persongroups' }}
                        options={this.state.groupOptions}
                        onChange={this.handleGroupChange}
                        value={this.state.person.groups.map((group) => { return group.id })}
                    />
                    <Form.TextArea
                        onChange={(event) => {
                            this.setState({
                                person: Object.assign({}, this.state.person, { comments: event.target.value }),
                            });
                        }}
                        value={this.state.person.comments}
                        placeholder='Comments'
                    />
                    <Button variant="contained" type="submit" fluid>
                        Save
                    </Button>
                </Form>
            </div >
        );
    }
}

export default PersonForm;