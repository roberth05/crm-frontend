/* eslint-env jest */

import PersonGroup from '../../../src/model/person-group';
import Person from '../../../src/model/person';
import { personForAPI } from '../../../src/api/people';

describe('Testing forAPI functions', () => {
    test('should return for API', () => {
        var testPerson = new Person();
        testPerson.firstName = 'Clay';
        testPerson.lastName = 'McDaniel';
        testPerson.id = '5cc2c50f42b2c32f981517ff'

        expect(personForAPI(testPerson)).toEqual({
            FirstName: 'Clay',
            LastName: 'McDaniel',
            _id: '5cc2c50f42b2c32f981517ff',
            Birthday: null,
            Comments: '',
            persongroups: []
        });
    })

    test('should return for API with Groups', () => {
        var testPerson = new Person();
        testPerson.firstName = 'Clay';
        testPerson.lastName = 'McDaniel';
        testPerson.id = '5cc2c50f42b2c32f981517ff'

        var testGroup = new PersonGroup();
        testGroup.id = ''
        testGroup.name = ''

        testPerson.groups = [testGroup]

        expect(personForAPI(testPerson)).toEqual({
            FirstName: 'Clay',
            LastName: 'McDaniel',
            _id: '5cc2c50f42b2c32f981517ff',
            Birthday: null,
            Comments: '',
            persongroups: ['']
        });
    });

    test('should return for API from first group', () => {
        var testPerson = new Person();
        testPerson.firstName = 'Clay';
        testPerson.lastName = 'McDaniel';
        testPerson.id = '5cc2c50f42b2c32f981517ff'

        var testGroup = new PersonGroup();
        testGroup.id = 'group1'
        testGroup.name = 'Group 1'

        testPerson.groups = [testGroup]

        expect(personForAPI(testPerson).persongroups[0]).toEqual('group1')
    });
})