/* eslint-env jest */


import PersonGroup from '../../src/model/person-group';
import Person from '../../src/model/person';
import TouchPoint from '../../src/model/touchpoint';
import addDays from 'date-fns/add_days';

const testJSON = {
    "_id": "5ce7c45ae6258b4de8f1776b",
    "FirstName": "Test",
    "LastName": "Again",
    "Birthday": null,
    "Comments": "Test comment",
    "createdAt": "2019-05-24T10:15:54.112Z",
    "updatedAt": "2019-05-24T10:15:54.266Z",
    "__v": 0,
    "Image": null,
    "persongroups": [
        { _id: 'testGroup1', Name: 'Test Group 1' }
    ],
    "touchpoints": [],
    "emailaddresses": [],
    "personroles": [],
    "id": "5ce7c45ae6258b4de8f1776b"
}

const testJSONFromState = {
    id: "5ce7c45ae6258b4de8f1776b",
    firstName: "Test",
    lastName: "Again",
    birthday: null,
    comments: "Test comment",
    imageUrl: '',
    persongroups: [
        'testGroup1'
    ],
    touchPoints: [],
    emailAddresses: [],
    personRoles: []
}

describe('Person tests', () => {
    test('should return correct person', () => {
        var testPerson = new Person();
        testPerson.firstName = 'Clay';
        testPerson.lastName = 'McDaniel';
        testPerson.id = '5cc2c50f42b2c32f981517ff'

        expect(testPerson.id).toEqual('5cc2c50f42b2c32f981517ff')
    });

    test('should return correct person from JSON', () => {
        const testPerson = Person.fromJS(testJSONFromState);

        expect(testPerson.id).toEqual(testJSONFromState.id)
        expect(testPerson.firstName).toEqual(testJSONFromState.firstName)
        expect(testPerson.lastName).toEqual(testJSONFromState.lastName)
        expect(testPerson.birthday).toEqual(testJSONFromState.birthday)
        expect(testPerson.comments).toEqual(testJSONFromState.comments)
        expect(testPerson.imageUrl).toEqual(testJSONFromState.imageUrl)
    });

    test('should return null birthday', () => {
        var testPerson = new Person(testJSON.id, testJSONFromState.firstName, testJSONFromState.lastName, testJSONFromState.birthday);

        expect(testPerson.birthday).toBeNull()
    });

    test('should serialize', () => {
        var testPerson = new Person();
        testPerson.firstName = 'Clay';
        testPerson.lastName = 'McDaniel';
        testPerson.id = '5cc2c50f42b2c32f981517ff';
        testPerson.birthday = new Date('2019-01-01');
        testPerson.comments = 'test comments'
        testPerson.addPersonGroup(new PersonGroup('testGroup1', 'Test Group 1'))

        expect(testPerson.toJSON()).toEqual({
            id: '5cc2c50f42b2c32f981517ff',
            firstName: 'Clay',
            lastName: 'McDaniel',
            birthday: new Date('2019-01-01T00:00:00.000Z'),
            comments: 'test comments',
            groups: [
                'testGroup1'
            ]
        })
    })

    test('should return the latest touchPoint', () => {
        let testPerson = Person.fromJS(testJSONFromState);

        expect(testPerson.id).toEqual(testJSONFromState.id)

        let tps = [
            new TouchPoint('tp1', 'Meeting', 'test comment 1', new Date(2019, 1, 1), false),
            new TouchPoint('tp2', 'Meeting', 'test comment 2', new Date(2019, 2, 1), false)
        ]

        testPerson.addTouchPoint(tps[0])
        testPerson.addTouchPoint(tps[1])

        expect(testPerson.latestTouchPoint).toBeInstanceOf(TouchPoint)
        expect(testPerson.latestTouchPoint.date).toEqual(new Date(2019, 2, 1))

    })

    test('should return the sorted groups', () => {
        let testPerson = Person.fromJS(testJSONFromState);

        expect(testPerson.id).toEqual(testJSONFromState.id)

        let tps = [
            new TouchPoint('tp1', 'Meeting', 'test comment 1', new Date(2019, 1, 1), false),
            new TouchPoint('tp2', 'Meeting', 'test comment 2', new Date(2019, 2, 1), false)
        ]

        let groups = [
            new PersonGroup('testGroup1', 'Test Group 1', 7),
            new PersonGroup('testGroup2', 'Test Group 2', 14)
        ]

        testPerson.addTouchPoint(tps[0])
        testPerson.addTouchPoint(tps[1])
        testPerson.addPersonGroup(groups[0])
        testPerson.addPersonGroup(groups[1])

        expect(testPerson.latestTouchPoint).toBeInstanceOf(TouchPoint)
        expect(testPerson.latestTouchPoint.date).toEqual(new Date(2019, 2, 1))

        expect(testPerson.sortedPersonGroups[0].contactFrequency).toEqual(7)

    })

    test('should return the deadline for next contact', () => {
        let testPerson = Person.fromJS(testJSONFromState);

        expect(testPerson.id).toEqual(testJSONFromState.id)

        let tps = [
            new TouchPoint('tp1', 'Meeting', 'test comment 1', new Date(2019, 1, 1), false),
            new TouchPoint('tp2', 'Meeting', 'test comment 2', new Date(2019, 2, 1), false)
        ]

        let groups = [
            new PersonGroup('testGroup1', 'Test Group 1', 7),
            new PersonGroup('testGroup2', 'Test Group 2', 14)
        ]

        testPerson.addTouchPoint(tps[0])
        testPerson.addTouchPoint(tps[1])
        testPerson.addPersonGroup(groups[0])
        testPerson.addPersonGroup(groups[1])

        expect(testPerson.latestTouchPoint).toBeInstanceOf(TouchPoint)
        expect(testPerson.latestTouchPoint.date).toEqual(new Date(2019, 2, 1))

        expect(testPerson.nextTouchPointBy).toEqual(new Date(2019, 2, 8))

    })

    test('should return the deadline for next contact with no existing touch points', () => {
        let testPerson = Person.fromJS(testJSONFromState);

        expect(testPerson.id).toEqual(testJSONFromState.id)


        let groups = [
            new PersonGroup('testGroup1', 'Test Group 1', 7),
            new PersonGroup('testGroup2', 'Test Group 2', 14)
        ]

        testPerson.addPersonGroup(groups[0])
        testPerson.addPersonGroup(groups[1])

        expect(testPerson.latestTouchPoint).toBeUndefined()

        expect(testPerson.nextTouchPointBy).toEqual(addDays(new Date(), 7))

    })
})