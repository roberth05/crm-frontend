import Company from "../../src/model/company";

const testCompaniesFromAPI = [
    { _id: 'company1', Name: 'Company 1', comments: 'Comment 1', Website: 'test1.org' },
    { _id: 'company2', Name: 'Company 2', comments: 'Comment 2', Website: 'test2.org' },
]

const testCompaniesFromState = [
    { id: 'company1', name: 'Company 1', comments: 'Comment 1', website: 'test1.org' },
    { id: 'company2', name: 'Company 2', comments: 'Comment 2', website: 'test2.org' },
]

describe('creation tests', () => {
    test('should create from constructor', () => {
        const company = new Company(
            testCompaniesFromState[0].id,
            testCompaniesFromState[0].name,
            testCompaniesFromState[0].website,
            testCompaniesFromState[0].comments
        )

        expect(company.id).toEqual(testCompaniesFromState[0].id)
        expect(company.name).toEqual(testCompaniesFromState[0].name)
        expect(company.comments).toEqual(testCompaniesFromState[0].comments)
        expect(company.website).toEqual(testCompaniesFromState[0].website)
    })

    test('should create from fromJS', () => {
        const company = Company.fromJS(testCompaniesFromState[0])
        expect(company.id).toEqual(testCompaniesFromState[0].id)
        expect(company.name).toEqual(testCompaniesFromState[0].name)
        expect(company.comments).toEqual(testCompaniesFromState[0].comments)
        expect(company.website).toEqual(testCompaniesFromState[0].website)
    })

    test('should create from fromAPI', () => {
        const company = Company.fromAPI(testCompaniesFromAPI[0])
        expect(company.id).toEqual(testCompaniesFromAPI[0]._id)
        expect(company.name).toEqual(testCompaniesFromAPI[0].Name)
        expect(company.comments).toEqual(testCompaniesFromAPI[0].comments)
        expect(company.website).toEqual(testCompaniesFromAPI[0].Website)
    })
})