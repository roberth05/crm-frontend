import ActionItem, { priorities } from '../../src/model/actionItem';
import TouchPoint from '../../src/model/touchpoint';

const testActions = [
    {
        id: 'testAction1', description: 'Description for Action 1', priority: 50, due: new Date(2020, 1, 1), complete: false,
        touchPoint: 'touchPoint1'
    },
    {
        id: 'testAction2', description: 'Description for Action 2', priority: 100, due: new Date(2021, 1, 1), complete: false,
        touchPoint: 'touchPoint1'
    },
    {
        id: 'testAction3', description: 'Description for Action 3', priority: 100, due: new Date(2020, 1, 1), complete: false
    },
    {
        id: 'testAction4', description: 'Description for Action 4', priority: 100, complete: false
    }
]

const testTouchPoints = [
    { id: 'touchPoint1', method: 'Phone', discussed: 'tp1 comment', date: '2019-01-01Z05:00:00', initiatedByMe: false, person: 'testPerson1' },
    { id: 'touchPoint2', method: 'Meeting', discussed: 'tp2 comment', date: '2019-01-02Z05:00:00', initiatedByMe: true, person: 'testPerson1' }
]

function createTestTouchPoint(touchPoint) {
    return new TouchPoint(touchPoint.id, touchPoint.method, touchPoint.discussed, touchPoint.date, touchPoint.initiatedByMe)
}

describe('Base model tests', () => {
    test('should create actionItem', () => {
        const testAction = new ActionItem('testAction1', 'Description for Action 1', priorities.HIGH, new Date(2020, 1, 1))
        expect(testAction.id).toEqual('testAction1')
        expect(testAction.description).toEqual('Description for Action 1')
        expect(testAction.priority).toEqual(100)
        expect(testAction.due).toEqual(new Date(2020, 1, 1))
        expect(testAction.complete).toEqual(false)
    })

    test('should create actionItem with touchPoint', () => {
        const testAction = new ActionItem('testAction1', 'Description for Action 1', priorities.HIGH, new Date(2020, 1, 1), false, createTestTouchPoint(testTouchPoints[0]))
        expect(testAction.id).toEqual('testAction1')
        expect(testAction.description).toEqual('Description for Action 1')
        expect(testAction.priority).toEqual(100)
        expect(testAction.due).toEqual(new Date(2020, 1, 1))
        expect(testAction.complete).toEqual(false)

        expect(testAction.touchPoint).toBeInstanceOf(TouchPoint)
        expect(testAction.touchPoint.id).toEqual('touchPoint1')
    })

    test('should create from JSON', () => {
        const testAction = ActionItem.fromJS(testActions[0])
        expect(testAction.id).toEqual('testAction1')
        expect(testAction.description).toEqual('Description for Action 1')
        expect(testAction.priority).toEqual(50)
        expect(testAction.due).toEqual(new Date(2020, 1, 1))
        expect(testAction.complete).toEqual(false)
    })

    test('should setTouchPoint', () => {
        const testAction = ActionItem.fromJS(testActions[1])
        testAction.setTouchPoint(createTestTouchPoint(testTouchPoints[1]));

        expect(testAction.touchPoint).toBeInstanceOf(TouchPoint)
        expect(testAction.touchPoint.id).toBe('touchPoint2')
        expect(testAction.touchPoint.actionItems.length).toEqual(1)
    })

    test('should sort by earliest due date first', () => {
        const testAction1 = ActionItem.fromJS(testActions[0])
        const testAction2 = ActionItem.fromJS(testActions[1])

        expect(testAction1.compare(testAction2)).toBeLessThan(0)
    })

    test('should sort by due date and highest priority first', () => {
        const testAction1 = ActionItem.fromJS(testActions[0])
        const testAction3 = ActionItem.fromJS(testActions[2])

        expect(testAction1.compare(testAction3)).toBeGreaterThan(0)
    })

    test('should sort by priority and then due date', () => {
        const testAction3 = ActionItem.fromJS(testActions[2])
        const testAction4 = ActionItem.fromJS(testActions[3])

        expect(testAction3.compare(testAction4)).toBeLessThan(0)
    })

    test('should sort by priority and then null due date', () => {
        const testAction2 = ActionItem.fromJS(testActions[1])
        const testAction4 = ActionItem.fromJS(testActions[3])

        expect(testAction2.compare(testAction4)).toBeLessThan(0)
    })

    test('should toggle Complete', () => {
        const testAction1 = ActionItem.fromJS(testActions[0])
        expect(testAction1.complete).toBe(false)
        testAction1.toggleComplete()
        expect(testAction1.complete).toBe(true)
    })
})

describe('ActionItem Serialization tests', () => {
    test('should serialize', () => {
        const testAction = ActionItem.fromJS(testActions[3])

        expect(testAction.toJSON()).toEqual({
            id: testActions[3].id,
            description: testActions[3].description,
            priority: testActions[3].priority,
            due: null,
            complete: testActions[3].complete,
            touchPoint: null
        })
    })

    test('should serialize with touchPoint', () => {
        const testAction = ActionItem.fromJS(testActions[1])
        testAction.setTouchPoint(createTestTouchPoint(testTouchPoints[1]));
        expect(testAction.toJSON()).toEqual({
            id: testActions[1].id,
            description: testActions[1].description,
            priority: testActions[1].priority,
            due: testActions[1].due,
            complete: testActions[1].complete,
            touchPoint: testTouchPoints[1].id
        })
    })

    test('should serialize with a null due date', () => {
        const testAction = ActionItem.fromJS(testActions[0])

        expect(testAction.toJSON()).toEqual({
            id: testActions[0].id,
            description: testActions[0].description,
            priority: testActions[0].priority,
            due: testActions[0].due,
            complete: testActions[0].complete,
            touchPoint: null
        })
    })
})