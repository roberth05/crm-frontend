import TouchPoint from "../../src/model/touchpoint";
import Person from '../../src/model/person';

const testTouchPointsFromAPI = [
    { _id: 'touchPoint1', Method: 'Phone', Discussed: 'tp1 comment', Date: '2019-01-01Z05:00:00', InitiatedByMe: false, person: 'testPerson1' },
    { _id: 'touchPoint2', Method: 'Meeting', Discussed: 'tp2 comment', Date: '2019-01-02Z05:00:00', InitiatedByMe: true, person: 'testPerson1' }
]

const testTouchPointsFromState = [
    { id: 'touchPoint1', method: 'Phone', discussed: 'tp1 comment', date: '2019-01-01Z05:00:00', initiatedByMe: false, person: 'testPerson1' },
    { id: 'touchPoint2', method: 'Meeting', discussed: 'tp2 comment', date: '2019-01-02Z05:00:00', initiatedByMe: true, person: 'testPerson1' }
]

const testPerson = [
    { id: 'testPerson1', firstName: 'Test', lastName: 'Person 1' }
]

function createTestPerson() {
    return new Person(
        testPerson[0].id, testPerson[0].firstName, testPerson[0].lastName
    )
}

describe('creation tests', () => {
    test('should create from constructor', () => {
        const touchPoint = new TouchPoint(
            testTouchPointsFromState[0].id,
            testTouchPointsFromState[0].method,
            testTouchPointsFromState[0].discussed,
            testTouchPointsFromState[0].date,
            testTouchPointsFromState[0].initiatedByMe,
            createTestPerson()
        )

        expect(touchPoint.id).toEqual(testTouchPointsFromState[0].id)
        expect(touchPoint.method).toEqual(testTouchPointsFromState[0].method)
        expect(touchPoint.discussed).toEqual(testTouchPointsFromState[0].discussed)
        expect(touchPoint.date).toEqual(testTouchPointsFromState[0].date)
        expect(touchPoint.initiatedByMe).toEqual(testTouchPointsFromState[0].initiatedByMe)
        expect(touchPoint.person.id).toEqual(testPerson[0].id)
    })

    test('should create with initiatedByMe false default value when supplied null', () => {
        const touchPoint = new TouchPoint(
            testTouchPointsFromState[0].id,
            testTouchPointsFromState[0].method,
            testTouchPointsFromState[0].discussed,
            testTouchPointsFromState[0].date,
            null,
            createTestPerson()
        )

        expect(touchPoint.id).toEqual(testTouchPointsFromState[0].id)
        expect(touchPoint.method).toEqual(testTouchPointsFromState[0].method)
        expect(touchPoint.discussed).toEqual(testTouchPointsFromState[0].discussed)
        expect(touchPoint.date).toEqual(testTouchPointsFromState[0].date)
        expect(touchPoint.initiatedByMe).toEqual(false)
        expect(touchPoint.person.id).toEqual(testPerson[0].id)
    })

    test('should create with initiatedByMe false default value when supplied undefined', () => {
        const touchPoint = new TouchPoint(
            testTouchPointsFromState[0].id,
            testTouchPointsFromState[0].method,
            testTouchPointsFromState[0].discussed,
            testTouchPointsFromState[0].date
        )

        expect(touchPoint.id).toEqual(testTouchPointsFromState[0].id)
        expect(touchPoint.method).toEqual(testTouchPointsFromState[0].method)
        expect(touchPoint.discussed).toEqual(testTouchPointsFromState[0].discussed)
        expect(touchPoint.date).toEqual(testTouchPointsFromState[0].date)
        expect(touchPoint.initiatedByMe).toEqual(false)
    })

    test('should create from fromJS', () => {
        const touchPoint = TouchPoint.fromJS(testTouchPointsFromState[0])
        expect(touchPoint.id).toEqual(testTouchPointsFromState[0].id)
        expect(touchPoint.name).toEqual(testTouchPointsFromState[0].name)
        expect(touchPoint.comments).toEqual(testTouchPointsFromState[0].comments)
        expect(touchPoint.website).toEqual(testTouchPointsFromState[0].website)
    })

    test('should create from fromAPI', () => {
        const touchPoint = TouchPoint.fromAPI(testTouchPointsFromAPI[0])
        expect(touchPoint.id).toEqual(testTouchPointsFromAPI[0]._id)
        expect(touchPoint.name).toEqual(testTouchPointsFromAPI[0].Name)
        expect(touchPoint.comments).toEqual(testTouchPointsFromAPI[0].comments)
        expect(touchPoint.website).toEqual(testTouchPointsFromAPI[0].Website)
    })
})

test('should serialize', () => {
    const touchPoint = new TouchPoint(
        testTouchPointsFromState[0].id,
        testTouchPointsFromState[0].method,
        testTouchPointsFromState[0].discussed,
        testTouchPointsFromState[0].date,
        testTouchPointsFromState[0].initiatedByMe,
        createTestPerson()
    )

    expect(touchPoint.toJSON()).toEqual(testTouchPointsFromState[0])
})