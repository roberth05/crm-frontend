import PersonRole from "../../src/model/person-role";
import Person from '../../src/model/person';
import Company from "../../src/model/company";

const testPersonRolesFromAPI = [
    {
        _id: '1234', Title: 'test 1', Started: new Date(2019, 1, 1), Ended: new Date(2019, 2, 1),
        company: {
            _id: 'company1', Name: 'Company 1'
        }, person: {
            _id: 'person1', FirstName: 'Person', LastName: '1'
        }
    },
    {
        _id: '5678', Title: 'test 2', Started: new Date(2018, 1, 1), Ended: new Date(2018, 2, 1),
        company: {
            _id: 'company2', Name: 'Company 2'
        }, person: {
            _id: 'person2', FirstName: 'Person', LastName: '2'
        }
    }
]

const testCompanies = [
    { id: 'company1', name: 'Company 2' },
    { id: 'company2', name: 'Company 2' }
]

const testPeople = [
    { id: 'person1', firstName: 'Person', lastName: '1' },
    { id: 'person2', firstName: 'Person', lastName: '2' }
]

function testPersonRolesFromState() {
    return testPersonRolesFromAPI.map((role) => {
        return {
            id: role._id,
            title: role.Title,
            started: role.Started,
            ended: role.Ended,
            company: role.company._id,
            person: role.person._id
        }
    })
}

function createTestPerson() {
    return new Person(
        testPeople[0].id, testPeople[0].firstName, testPeople[0].lastName
    )
}

function createTestCompany() {
    return new Company(
        testCompanies[0].id, testCompanies[0].name
    )
}

describe('creation tests', () => {
    test('should create from constructor', () => {
        const personRole = new PersonRole(
            testPersonRolesFromState()[0].id,
            testPersonRolesFromState()[0].title,
            testPersonRolesFromState()[0].started,
            testPersonRolesFromState()[0].ended,
            createTestPerson(),
            createTestCompany()
        )

        expect(personRole.id).toEqual(testPersonRolesFromState()[0].id)
        expect(personRole.title).toEqual(testPersonRolesFromState()[0].title)
        expect(personRole.started).toEqual(testPersonRolesFromState()[0].started)
        expect(personRole.ended).toEqual(testPersonRolesFromState()[0].ended)
        expect(personRole.company.id).toEqual(testCompanies[0].id)
        expect(personRole.person.id).toEqual(testPeople[0].id)
    })

    test('should create from fromJS', () => {
        const personRole = PersonRole.fromJS(testPersonRolesFromState()[0])
        expect(personRole.id).toEqual(testPersonRolesFromState()[0].id)
        expect(personRole.title).toEqual(testPersonRolesFromState()[0].title)
        expect(personRole.started).toEqual(testPersonRolesFromState()[0].started)
        expect(personRole.ended).toEqual(testPersonRolesFromState()[0].ended)
        //TODO: Need to determine what the intended action is
        /* 
        expect(personRole.person.id).toEqual(testPeople[0].id)
        expect(personRole.company.id).toEqual(testCompanies[0].id)
        */
    })
})

test('should serialize', () => {
    const personRole = new PersonRole(
        testPersonRolesFromState()[0].id,
        testPersonRolesFromState()[0].title,
        testPersonRolesFromState()[0].started,
        testPersonRolesFromState()[0].ended,
        createTestPerson(),
        createTestCompany()
    )

    expect(personRole.toJSON()).toEqual(testPersonRolesFromState()[0])
})

describe('comarison functions', () => {
    test('should compare as more recent', () => {
        const role1 = new PersonRole(
            testPersonRolesFromState()[0].id,
            testPersonRolesFromState()[0].title,
            testPersonRolesFromState()[0].started,
            testPersonRolesFromState()[0].ended,
            createTestPerson(),
            createTestCompany()
        )
        const role2 = new PersonRole(
            testPersonRolesFromState()[1].id,
            testPersonRolesFromState()[1].title,
            testPersonRolesFromState()[1].started,
            testPersonRolesFromState()[1].ended,
            createTestPerson(),
            createTestCompany()
        )

        expect(role1.started).toEqual(new Date(2019, 1, 1))
        expect(role2.started).toEqual(new Date(2018, 1, 1))

        expect(role1.compare(role2)).toEqual(-1)
    })

    test('should compare as less recent', () => {
        const role1 = new PersonRole(
            testPersonRolesFromState()[0].id,
            testPersonRolesFromState()[0].title,
            testPersonRolesFromState()[0].started,
            testPersonRolesFromState()[0].ended,
            createTestPerson(),
            createTestCompany()
        )
        const role2 = new PersonRole(
            testPersonRolesFromState()[1].id,
            testPersonRolesFromState()[1].title,
            testPersonRolesFromState()[1].started,
            testPersonRolesFromState()[1].ended,
            createTestPerson(),
            createTestCompany()
        )

        expect(role1.started).toEqual(new Date(2019, 1, 1))
        expect(role2.started).toEqual(new Date(2018, 1, 1))

        expect(role2.compare(role1)).toEqual(1)
    })

    test('should compare as the same', () => {
        const role1 = new PersonRole(
            testPersonRolesFromState()[0].id,
            testPersonRolesFromState()[0].title,
            testPersonRolesFromState()[0].started,
            testPersonRolesFromState()[0].ended,
            createTestPerson(),
            createTestCompany()
        )
        const role2 = new PersonRole(
            testPersonRolesFromState()[1].id,
            testPersonRolesFromState()[1].title,
            testPersonRolesFromState()[0].started,
            testPersonRolesFromState()[0].ended,
            createTestPerson(),
            createTestCompany()
        )

        expect(role1.started).toEqual(new Date(2019, 1, 1))
        expect(role2.started).toEqual(new Date(2019, 1, 1))

        expect(role2.compare(role1)).toEqual(0)
    })

    test('should compare as more recent based on end date', () => {
        const role1 = new PersonRole(
            testPersonRolesFromState()[0].id,
            testPersonRolesFromState()[0].title,
            testPersonRolesFromState()[0].started,
            testPersonRolesFromState()[0].ended,
            createTestPerson(),
            createTestCompany()
        )
        const role2 = new PersonRole(
            testPersonRolesFromState()[1].id,
            testPersonRolesFromState()[1].title,
            testPersonRolesFromState()[0].started,
            new Date(2019, 1, 10),
            createTestPerson(),
            createTestCompany()
        )

        expect(role1.ended).toEqual(new Date(2019, 2, 1))
        expect(role2.ended).toEqual(new Date(2019, 1, 10))

        expect(role1.compare(role2)).toEqual(-1)
    })
}
)