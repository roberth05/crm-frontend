/* eslint-env jest */


import PersonGroup from '../../src/model/person-group';
import PersonGroupStore from '../../src/stores/person-group-store';
import { initializeStore } from '../../src/stores/stores';

const testGroups = [
    { _id: '1234', Name: 'test 1', contactFrequency: 7 },
    { _id: '5678', Name: 'test 2', contactFrequency: 30 }
]

function testGroupsFromState() {
    return testGroups.map((group) => {
        return { id: group._id, name: group.Name, contactFrequency: group.contactFrequency }
    })
}

test('should load with rootStore', () => {
    const { rootStore } = initializeStore({});
    expect(rootStore.personGroupStore).toBeInstanceOf(PersonGroupStore)
    expect(rootStore.personGroupStore.rootStore).toBe(rootStore)
})

test('should load from intializedState', async () => {
    const store = new PersonGroupStore({ personGroups: testGroupsFromState() });
    expect(store.personGroups.length).toEqual(2)
    expect(store.personGroups[0].id).toEqual('1234')
})

test('should return correct count', () => {
    var group = new PersonGroup()
    group.id = '1234';
    group.name = 'test Group'

    var store = new PersonGroupStore();
    store.addPersonGroup(group);

    expect(group.id).toEqual('1234');

    expect(store.personGroups.length).toEqual(1);
});

test('should load 2', () => {
    var store = new PersonGroupStore();

    testGroups.forEach(group => {
        store.addPersonGroup(new PersonGroup(group._id, group.Name, group.contactFrequency));
    });
    expect(store.personGroups.length).toEqual(2);
});

test('should not add twice', () => {
    var store = new PersonGroupStore();

    testGroups.forEach(group => {
        store.addPersonGroup(new PersonGroup(group._id, group.Name, group.contactFrequency));
    });
    expect(store.personGroups.length).toEqual(2);

    store.addPersonGroup(new PersonGroup(testGroups[0]._id, testGroups[0].Name, testGroups[0].contactFrequency));
    expect(store.personGroups.length).toEqual(2);
});

test('should serialize', () => {
    var store = new PersonGroupStore();
    testGroups.forEach(group => {
        store.addPersonGroup(new PersonGroup(group._id, group.Name, group.contactFrequency));
    });

    expect(store.toJSON()).toEqual({
        personGroups: [
            { id: '1234', name: 'test 1', contactFrequency: 7 },
            { id: '5678', name: 'test 2', contactFrequency: 30 }
        ]
    })
})