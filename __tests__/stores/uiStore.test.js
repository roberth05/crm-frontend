import UIStore from "../../src/stores/ui-store";

test('should serialize', () => {
    const store = new UIStore()

    expect(store.toJSON()).toEqual({
        sidebarOpen: false,
        newPersonOpen: false,
        newTouchPointOpen: false,
        newRoleOpen: false,
        newActionItemOpen: false,
        selectedPerson: null,
        selectedCompany: null
    })
})