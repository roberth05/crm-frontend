/* eslint-env jest */


import PersonRole from '../../src/model/person-role';
import RoleStore from '../../src/stores/roleStore';
import Person from '../../src/model/person';
import RootStore, { initializeStore } from '../../src/stores/stores';

import * as rolesAPI from '../../src/api/personRoles';
import Company from '../../src/model/company';

//jest.mock(rolesAPI);

const testRoles = [
    {
        _id: '1234', Title: 'test 1',
        company: {
            _id: 'company1', Name: 'Company 1'
        }, person: {
            _id: 'person1', FirstName: 'Person', LastName: '1'
        }
    },
    {
        _id: '5678', Title: 'test 2',
        company: {
            _id: 'company2', Name: 'Company 2'
        }, person: {
            _id: 'person2', FirstName: 'Person', LastName: '2'
        }
    }
]

const testCompanies = [
    { id: 'company1', name: 'Company 2' },
    { id: 'company2', name: 'Company 2' }
]

const testPeople = [
    { id: 'person1', firstName: 'Person', lastName: '1' },
    { id: 'person2', firstName: 'Person', lastName: '2' }
]

function testRolesFromState() {
    return testRoles.map((role) => {
        return {
            id: role._id,
            title: role.title,
            company: role.company._id,
            person: role.person._id
        }
    })
}

test('should load with rootStore', () => {
    const { rootStore } = initializeStore({});
    expect(rootStore.roleStore).toBeInstanceOf(RoleStore)
    expect(rootStore.roleStore.rootStore).toBe(rootStore)
})

test('should return correct count', () => {
    var role = new PersonRole()
    role.id = '1234';
    role.title = 'test role'

    var store = new RoleStore();
    store.addRole(role);

    expect(role.id).toEqual('1234');

    expect(store.roles.length).toEqual(1);
});

test('should load 2', () => {
    var store = new RoleStore();

    testRoles.forEach(role => {
        store.addRole(new PersonRole(role));
    });
    expect(store.roles.length).toEqual(2);
});

test('should not add twice', () => {
    var store = new RoleStore();

    testRoles.forEach(role => {
        store.addRole(new PersonRole(role));
    });
    expect(store.roles.length).toEqual(2);

    store.addRole(new PersonRole(testRoles[0]));
    expect(store.roles.length).toEqual(2);
});

test('should serialize', () => {
    var store = new RoleStore();
    testRoles.forEach(role => {
        store.addRole(new PersonRole(role._id, role.Title, undefined, undefined, null, new Company(role.company._id, role.company.Name)));
    });
    expect(store.toJSON()).toEqual({
        roles: [
            { id: '1234', title: 'test 1', started: undefined, ended: undefined, company: 'company1', person: null },
            { id: '5678', title: 'test 2', started: undefined, ended: undefined, company: 'company2', person: null }
        ]
    })
})

describe('tests for intializing from state', () => {
    test('should initialize from state', () => {
        const root = new RootStore({
            roleStore: { roles: testRolesFromState() },
            peopleStore: { people: testPeople },
            companyStore: { companies: testCompanies }
        })
        const store = root.roleStore;
        expect(store.roles.length).toEqual(2)
    })

    test('should load from intializedState with people & company references', async () => {
        const root = new RootStore({
            roleStore: { roles: testRolesFromState() },
            peopleStore: { people: testPeople },
            companyStore: { companies: testCompanies }
        })

        const store = root.roleStore

        expect(store.roles.length).toEqual(2)
        expect(store.roles[0].person).toBeInstanceOf(Person)
        expect(store.roles[0].company).toBeInstanceOf(Company)
    })
})

describe('API calls', () => {
    test('FetchAllForPerson', async () => {
        rolesAPI.getPersonRolesForPerson = jest.fn(() => {
            return Promise.resolve(testRoles)
        });

        var store = new RoleStore({}, rolesAPI);

        var testPerson = new Person();
        testPerson.firstName = 'Clay';
        testPerson.lastName = 'McDaniel';
        testPerson.id = '5cc2c50f42b2c32f981517ff'

        await store.fetchAllForPerson(testPerson);

        expect(rolesAPI.getPersonRolesForPerson).toHaveBeenCalled();
        expect(store.roles.length).toEqual(2);
        expect(store.roles[0].company).toBeDefined();

    })

    test('get Roles only for person', async () => {
        rolesAPI.getPersonRolesForPerson = jest.fn(() => {
            return Promise.resolve(testRolesFromState())
        });

        var store = new RoleStore({}, new RootStore({
            peopleStore: { people: testPeople },
            companyStore: { companies: testCompanies }
        }), rolesAPI);

        var testPerson2 = new Person('person1', 'Test', 'Tester');

        await store.loadRolesForPerson(testPerson2);

        // store.addRole(testRole);

        expect(rolesAPI.getPersonRolesForPerson).toHaveBeenCalled();
        expect(store.roles.length).toEqual(2);

        const roles = store.findByPerson(testPerson2);
        expect(roles.length).toEqual(1);
    })

    test('get Roles only for company', async () => {
        rolesAPI.getPersonRolesForCompany = jest.fn(() => {
            return Promise.resolve(testRolesFromState())
        });

        var store = new RoleStore({}, new RootStore({
            peopleStore: { people: testPeople },
            companyStore: { companies: testCompanies }
        }), rolesAPI);

        var testCompany = new Company('company1', 'Test', 'Tester');

        await store.loadRolesForCompany(testCompany);

        expect(rolesAPI.getPersonRolesForCompany).toHaveBeenCalled();
        expect(store.roles.length).toEqual(2);

        const roles = store.findByCompany(testCompany);
        expect(roles.length).toEqual(1);
    })
})