/* eslint-env jest */


import PeopleStore from '../../src/stores/people-store';
import Person from '../../src/model/person';
import RootStore, { initializeStore } from '../../src/stores/stores';
import * as PeopleAPI from '../../src/api/people';
import PersonGroupStore from '../../src/stores/person-group-store';
import PersonGroup from '../../src/model/person-group';
import TouchPointStore from '../../src/stores/touchPoint-store';
import TouchPoint from '../../src/model/touchpoint';
jest.mock('../../src/api/people');
jest.mock('../../src/stores/person-group-store');
jest.mock('../../src/stores/touchPoint-store');

const testPeople = [
    {
        _id: '1234', FirstName: 'Test', LastName: '1', persongroups: ['1234', '5678']
    },
    {
        _id: '5678', FirstName: 'Test', LastName: '2', persongroups: ['5678']
    }
]

const testGroups = [
    { _id: '1234', Name: 'test 1', contactFrequency: 7 },
    { _id: '5678', Name: 'test 2', contactFrequency: 14 }
]

function testPeopleFromState() {
    return testPeople.map((person) => {
        let retVal = {
            id: person._id,
            firstName: person.FirstName,
            lastName: person.LastName,
            groups: []
        }
        person.persongroups.forEach(group => {
            retVal.groups.push(group)
        });
        return retVal
    })
}

function testGroupsFromState() {
    return testGroups.map((group) => {
        return { id: group._id, name: group.Name, contactFrequency: group.contactFrequency }
    })
}

const testTouchPointsFromState = [
    { id: 'touchPoint1', method: 'Phone', discussed: 'tp1 comment', date: '2019-01-01Z05:00:00', initiatedByMe: false, person: '1234' },
    { id: 'touchPoint2', method: 'Meeting', discussed: 'tp2 comment', date: '2019-01-02Z05:00:00', initiatedByMe: true, person: '1234' }
]

beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    PersonGroupStore.mockClear();
    PersonGroupStore.mockImplementation(() => {
        const groups = testGroups.map((group) => { return new PersonGroup(group._id, group.Name, group.contactFrequency) })
        return {
            find: (groupId) => {
                // console.log(`Mock find called: ${groupId}`)
                return groups.find((group) => {
                    // console.log(`${group.id}-${groupId}`)
                    return group.id === groupId
                })
            },
        };
    });

    // TouchPointStore.mockClear();
    // TouchPointStore.mockImplementation(() => {
    //     const tps = testTouchPointsFromState.map((tp) => { return new TouchPoint(tp.id, tp.method, tp.discussed, tp.date, tp.initiatedByMe) })
    //     return {

    //     }
    // })
});

test('should be created with RootStore reference', () => {
    const { rootStore } = initializeStore({})
    expect(rootStore.peopleStore).toBeInstanceOf(PeopleStore)
    expect(rootStore.peopleStore.rootStore).toBe(rootStore)
})

test('should load from intializedState', async () => {
    const root = new RootStore({
        // personGroupStore: { personGroups: testGroupsFromState() },
        peopleStore: { people: testPeopleFromState() }
    })
    // const store = new PeopleStore({ people: testPeopleFromState() });
    const store = root.peopleStore;
    // expect(root.personGroupStore.personGroups.length).toBe(2)
    expect(store.people.length).toEqual(2)
    expect(store.people[0]).toBeInstanceOf(Person)
    expect(store.people[0].id).toEqual('1234')
    expect(store.people[0].groups.length).toEqual(2)
})

test('should return sorted array', async () => {
    const root = new RootStore({})
    const store = new PeopleStore({ people: testPeopleFromState() }, root);
    expect(store.people.length).toEqual(2)
    expect(store.people[0]).toBeInstanceOf(Person)
    expect(store.sortedPeopleAsc[0].id).toEqual('1234')
    expect(store.sortedPeopleDesc[0].id).toEqual('5678')
})

test('should return correct count', () => {
    var person = new Person()
    person.id = '1234';
    person.title = 'test person'

    var store = new PeopleStore();
    store.addPerson(person);

    expect(person.id).toEqual('1234');

    expect(store.people.length).toEqual(1);
});

test('should load 2', () => {
    var store = new PeopleStore();

    testPeople.forEach(person => {
        store.addPerson(new Person(person));
    });
    expect(store.people.length).toEqual(2);
});

test('should load 1', async () => {
    PeopleAPI.getPerson = jest.fn(() => {
        return testPeopleFromState()[0]
    })

    var store = new PeopleStore({}, new RootStore({}));
    expect(store.people.length).toEqual(0);

    const person = await store.loadPerson('1234')
    expect(PeopleAPI.getPerson).toBeCalled();
    expect(person.id).toEqual('1234')
    expect(person).toBeInstanceOf(Person)
    expect(store.people.length).toEqual(1);
});

test('should load 2 using loadPeople', async () => {
    PeopleAPI.getPeopleList = jest.fn(() => {
        return testPeopleFromState();
    })

    const { rootStore } = initializeStore({});
    const store = rootStore.peopleStore;

    await store.loadPeople();

    expect(store.people.length).toEqual(2)
    expect(store.people[0]).toBeInstanceOf(Person)
    expect(store.people[0].groups.length).toEqual(2)
})

test('should not add twice', () => {
    var store = new PeopleStore();

    testPeople.forEach(person => {
        store.addPerson(new Person(person));
    });
    expect(store.people.length).toEqual(2);

    store.addPerson(new Person(testPeople[0]));
    expect(store.people.length).toEqual(2);
});

test('should be able to find using id', () => {
    var store = new PeopleStore();

    testPeople.forEach(person => {
        store.addPerson(new Person(person._id, person.FirstName, person.LastName));
    });
    expect(store.people.length).toEqual(2);

    const person = store.find('1234')
    expect(person.id).toEqual('1234')
    expect(person).toBeInstanceOf(Person)
});

test('should serialize', () => {
    var store = new PeopleStore();
    expect(store.toJSON()).toEqual({ people: [] })
})