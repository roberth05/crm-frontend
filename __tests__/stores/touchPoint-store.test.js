import RootStore, { initializeStore } from "../../src/stores/stores";
import TouchPointStore from "../../src/stores/touchPoint-store";
import TouchPoint from '../../src/model/touchpoint';
import * as touchPointAPI from '../../src/api/touchpoints';
import PeopleStore from '../../src/stores/people-store';
import Person from "../../src/model/person";
jest.mock('../../src/stores/people-store');

const testTouchPointsFromAPI = [
    { _id: 'touchPoint1', Method: 'Phone', Discussed: 'tp1 comment', Date: '2019-01-01Z05:00:00', InitiatedByMe: false, person: 'testPerson1' },
    { _id: 'touchPoint2', Method: 'Meeting', Discussed: 'tp2 comment', Date: '2019-01-02Z05:00:00', InitiatedByMe: true, person: 'testPerson1' }
]
const testTouchPointsFromState = [
    { id: 'touchPoint1', method: 'Phone', discussed: 'tp1 comment', date: '2019-01-01Z05:00:00', initiatedByMe: false, person: 'testPerson1' },
    { id: 'touchPoint2', method: 'Meeting', discussed: 'tp2 comment', date: '2019-01-02Z05:00:00', initiatedByMe: true, person: 'testPerson1' }
]
const testPeople = [
    { _id: 'testPerson1', FirstName: 'Test', LastName: 'Person 1' },
    { _id: 'testPerson2', FirstName: 'Test', LastName: 'Person 2' },
]

const newTouchPointFromAPI = [
    { id: 'newTouchPointAPI', method: 'Meeting', discussed: 'newTP comment', date: '2019-01-03Z05:00:00', initiatedByMe: true, person: 'testPerson2' }
]

beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    PeopleStore.mockClear();
    PeopleStore.mockImplementation(() => {
        const people = testPeople.map((person) => { return new Person(person._id, person.FirstName, person.LastName) })
        return {
            find: (personId) => {
                // console.log(personId)
                return people.find((person) => {
                    // console.log(`${person.id}-${personId}`)
                    return person.id === personId
                })
            },
        };
    });
});

test('should load with rootStore', () => {
    const { rootStore } = initializeStore({});
    expect(rootStore.touchPointStore).toBeInstanceOf(TouchPointStore)
    expect(rootStore.touchPointStore.rootStore).toBe(rootStore)
})

test('should serialize properly', () => {
    const { rootStore } = initializeStore({})
    const store = rootStore.touchPointStore;

    expect(store.toJSON()).toEqual({ touchPoints: [] })
})

test('should serialize array properly', () => {
    const { rootStore } = initializeStore({})
    const store = rootStore.touchPointStore;

    store.addTouchPoint(new TouchPoint(
        testTouchPointsFromState[0].id,
        testTouchPointsFromState[0].method,
        testTouchPointsFromState[0].discussed,
        testTouchPointsFromState[0].date,
        testTouchPointsFromState[0].initiatedByMe,
        new Person(testTouchPointsFromState[0].person)
    ))
    expect(store.toJSON()).toEqual({
        touchPoints: [
            { id: 'touchPoint1', method: 'Phone', discussed: 'tp1 comment', date: '2019-01-01Z05:00:00', initiatedByMe: false, person: 'testPerson1' }
        ]
    })
})

test('should sort touchPoints', () => {
    const store = new TouchPointStore({
        touchPoints: [
            testTouchPointsFromState[0], testTouchPointsFromState[1]
        ]
    })

    expect(store.touchPoints.length).toEqual(2)
    expect(store.touchPoints[0].id).toEqual('touchPoint1')
    expect(store.sortedTouchPoints[0].id).toEqual('touchPoint2')
})

describe('loading functions', () => {
    test('should load from API', async () => {
        touchPointAPI.getTouchPointsList = jest.fn(() => {
            return Promise.resolve(testTouchPointsFromState)
        })

        const rootStore = new RootStore({});
        const store = rootStore.touchPointStore
        expect(store.touchPoints.length).toEqual(0)

        await store.loadTouchPoints()
        expect(store.touchPoints.length).toEqual(2)
    })

    test('should load from intializedState', async () => {
        const store = new TouchPointStore({ touchPoints: testTouchPointsFromState });
        expect(store.touchPoints.length).toEqual(2)
    })

    test('should load from intializedState with people references', async () => {
        const root = new RootStore({ touchPointStore: { touchPoints: testTouchPointsFromState } })
        // const store = new TouchPointStore({ touchPoints: testTouchPointsFromState });
        const store = root.touchPointStore

        expect(store.touchPoints.length).toEqual(2)
        expect(store.touchPoints[0].person).toBeInstanceOf(Person)
    })

    test('should load people at same time', async () => {
        touchPointAPI.getTouchPointsList = jest.fn(() => {
            return Promise.resolve(testTouchPointsFromState)
        })

        // PeopleStore.findPerson.mockResolvedValue(people[0])

        const rootStore = new RootStore({});
        const store = rootStore.touchPointStore
        expect(store.touchPoints.length).toEqual(0)
        expect(PeopleStore).toHaveBeenCalledTimes(1)

        await store.loadTouchPoints()
        expect(store.touchPoints.length).toEqual(2)
        expect(store.touchPoints[0].person).toBeTruthy()
        expect(store.touchPoints[0].person.id).toEqual('testPerson1')
    })
})

describe('crud functions', () => {
    test('should add new', async () => {
        touchPointAPI.addNewTouchPoint = jest.fn(() => {
            return Promise.resolve(newTouchPointFromAPI[0])
        })
        const root = new RootStore({ commonStore: { token: 'testToken' } })
        // root.commonStore.setToken('testToken')

        const store = root.touchPointStore
        let newTouchPoint = new TouchPoint(
            'thisShouldNotPersist',
            newTouchPointFromAPI[0].name,
            newTouchPointFromAPI[0].website,
            newTouchPointFromAPI[0].comments
        )
        expect(store.touchPoints.length).toEqual(0)
        //TODO: Make this work
        await store.saveNew(newTouchPoint)
        expect(touchPointAPI.addNewTouchPoint).toBeCalledWith(newTouchPoint, expect.stringMatching('testToken'))
        expect(store.touchPoints.length).toEqual(1)
        expect(store.touchPoints.find((touchPoint) => touchPoint.id === 'newTouchPointAPI')).toBeInstanceOf(TouchPoint)
    })

    test('should delete', async () => {
        touchPointAPI.deleteTouchPoint = jest.fn(async () => {
            return Promise.resolve('')
        })

        const root = new RootStore({ touchPointStore: { touchPoints: testTouchPointsFromState } })
        // const store = new TouchPointStore({ touchPoints: testTouchPointsFromState });
        const store = root.touchPointStore

        expect(store.touchPoints.length).toEqual(2)

        await store.deleteTouchPoint('touchPoint1')
        expect(store.touchPoints.length).toEqual(1)
    })
})