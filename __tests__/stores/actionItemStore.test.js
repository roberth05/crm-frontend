import RootStore, { initializeStore } from "../../src/stores/stores";
import ActionItemStore from '../../src/stores/actionItem-store';
import * as actionItemAPI from '../../src/api/actionItems';
import ActionItem from "../../src/model/actionItem";
import TouchPoint from "../../src/model/touchpoint";

const testActions = [
    {
        id: 'testAction1', description: 'Description for Action 1', priority: 50, due: new Date(2020, 1, 1), complete: false, touchPoint: 'touchPoint1'
    },
    {
        id: 'testAction2', description: 'Description for Action 2', priority: 100, due: new Date(2021, 1, 1), complete: false, touchPoint: 'touchPoint1'
    },
    {
        id: 'testAction3', description: 'Description for Action 3', priority: 100, due: new Date(2020, 1, 1), complete: false, touchPoint: 'touchPoint2'
    },
    {
        id: 'testAction4', description: 'Description for Action 4', priority: 100, due: null, complete: false, touchPoint: null
    }
]

const testTouchPoints = [
    { id: 'touchPoint1', method: 'Phone', discussed: 'tp1 comment', date: '2019-01-01Z05:00:00', initiatedByMe: false },
    { id: 'touchPoint2', method: 'Meeting', discussed: 'tp2 comment', date: '2019-01-02Z05:00:00', initiatedByMe: true }
]

const newActionFromAPI = [
    {
        id: 'newActionItemAPI', description: 'Description for Action 1', priority: 50, due: new Date(2020, 1, 1), complete: false
    }
]

const editedActionFromAPI = [
    {
        id: 'testAction1', description: 'Description for Action 1 - edited', priority: 50, due: new Date(2020, 1, 1), complete: true
    }
]

describe('ActionItemStore base functions', () => {
    test('should load with rootStore', () => {
        const { rootStore } = initializeStore({});
        expect(rootStore.actionItemStore).toBeInstanceOf(ActionItemStore)
        expect(rootStore.actionItemStore.rootStore).toBe(rootStore)
    })

    test('should load with initial data', () => {
        const store = new ActionItemStore({
            actionItems: testActions
        })

        expect(store.actionItems.length).toEqual(4)
        expect(store.actionItems[0].id).toEqual(testActions[0].id)
    })

    test('should load from intializedState with touchPoint references', async () => {
        const root = new RootStore({
            actionItemStore: { actionItems: testActions },
            touchPointStore: { touchPoints: testTouchPoints }
        })

        const store = root.actionItemStore

        expect(store.actionItems.length).toEqual(4)
        expect(store.actionItems[0].touchPoint).toBeInstanceOf(TouchPoint)
    })

    test('should find correct action', () => {
        const store = new ActionItemStore({
            actionItems: testActions
        })

        expect(store.actionItems.length).toEqual(4)
        expect(store.actionItems[0].id).toEqual(testActions[0].id)

        const retVal = store.find(testActions[0].id)
        expect(retVal).toBeInstanceOf(ActionItem)
        expect(retVal.id).toEqual(testActions[0].id)
    })
})

describe('ActionItemStore serialization', () => {
    test('should serialize', () => {
        const store = new ActionItemStore({
            actionItems: [{
                id: 'testAction1', description: 'Description for Action 1', priority: 50, due: new Date(2020, 1, 1), complete: false, touchPoint: null
            },
            {
                id: 'testAction2', description: 'Description for Action 2', priority: 100, due: new Date(2021, 1, 1), complete: false, touchPoint: null
            },
            {
                id: 'testAction3', description: 'Description for Action 3', priority: 100, due: new Date(2020, 1, 1), complete: false, touchPoint: null
            },
            {
                id: 'testAction4', description: 'Description for Action 4', priority: 100, due: null, complete: false, touchPoint: null
            }]
        })

        expect(store.toJSON()).toEqual({
            actionItems: [{
                id: 'testAction1', description: 'Description for Action 1', priority: 50, due: new Date(2020, 1, 1), complete: false, touchPoint: null
            },
            {
                id: 'testAction2', description: 'Description for Action 2', priority: 100, due: new Date(2021, 1, 1), complete: false, touchPoint: null
            },
            {
                id: 'testAction3', description: 'Description for Action 3', priority: 100, due: new Date(2020, 1, 1), complete: false, touchPoint: null
            },
            {
                id: 'testAction4', description: 'Description for Action 4', priority: 100, due: null, complete: false, touchPoint: null
            }]
        })
    })
})

describe('ActionItems should sort', () => {
    test('should sort by due date first, then priority, then blank or equal', () => {
        const store = new ActionItemStore({
            actionItems: testActions
        })

        const sorted = store.sortedActionItems;

        expect(sorted[0].id).toEqual('testAction3')
        expect(sorted[1].id).toEqual('testAction1')
        expect(sorted[2].id).toEqual('testAction2')
        expect(sorted[3].id).toEqual('testAction4')
    })
})

describe('ActionItems API related functions', () => {
    test('ActionItems should load from API', async () => {
        actionItemAPI.getActionItemList = jest.fn(() => {
            return Promise.resolve(testActions)
        })

        const rootStore = new RootStore({});
        const store = rootStore.actionItemStore
        expect(store.actionItems.length).toEqual(0)

        await store.loadActionItems()
        expect(store.actionItems.length).toEqual(4)
    })

    test('should add new', async () => {
        actionItemAPI.addNewActionItem = jest.fn(() => {
            return Promise.resolve(newActionFromAPI[0])
        })
        const root = new RootStore({ commonStore: { token: 'testToken' } })

        const store = root.actionItemStore
        let newActionItem = new ActionItem(
            'thisShouldNotPersist',
            newActionFromAPI[0].description,
            newActionFromAPI[0].priority,
            newActionFromAPI[0].due,
            false
        )

        expect(store.actionItems.length).toEqual(0)
        const returnedVal = await store.saveNew(newActionItem)
        expect(actionItemAPI.addNewActionItem).toBeCalledWith(newActionItem, expect.stringMatching('testToken'))
        expect(store.actionItems.length).toEqual(1)
        expect(returnedVal.id).toEqual('newActionItemAPI')
        expect(returnedVal).toBeInstanceOf(ActionItem)
        expect(store.actionItems.find((actionItem) => actionItem.id === 'newActionItemAPI')).toBeInstanceOf(ActionItem)
    })

    test('should save existing', async () => {
        actionItemAPI.saveActionItem = jest.fn(() => {
            return Promise.resolve(editedActionFromAPI[0])
        })
        actionItemAPI.getActionItemList = jest.fn(() => {
            return Promise.resolve(testActions)
        })

        const root = new RootStore({ commonStore: { token: 'testToken' } })

        const store = root.actionItemStore
        expect(store.actionItems.length).toEqual(0)

        await store.loadActionItems()
        expect(store.actionItems.length).toEqual(4)

        let thisActionItem = store.actionItems[0]
        thisActionItem.complete = true;
        thisActionItem.description = 'Description for Action 1 - edited';
        const returnedVal = await store.save(thisActionItem)
        expect(actionItemAPI.saveActionItem).toBeCalledWith(thisActionItem, expect.stringMatching('testToken'))
        expect(store.actionItems.length).toEqual(4)
        expect(returnedVal.id).toEqual('testAction1')
        expect(returnedVal.complete).toEqual(true)
        expect(returnedVal).toBeInstanceOf(ActionItem)
        expect(store.actionItems.find((actionItem) => actionItem.id === 'testAction1')).toBeInstanceOf(ActionItem)
        expect(store.actionItems.find((actionItem) => actionItem.id === 'testAction1').id).toEqual('testAction1')
        expect(store.actionItems.find((actionItem) => actionItem.id === 'testAction1').complete).toEqual(true)
    })

    test('should delete', async () => {
        actionItemAPI.deleteActionItem = jest.fn(async () => {
            return Promise.resolve('')
        })

        const root = new RootStore({ actionItemStore: { actionItems: testActions } })
        const store = root.actionItemStore

        expect(store.actionItems.length).toEqual(4)

        await store.deleteActionItem('testAction1')
        expect(store.actionItems.length).toEqual(3)
    })
})