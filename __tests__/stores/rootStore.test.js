import RootStore, { initializeStore, isServer } from "../../src/stores/stores";
import TouchPointStore from "../../src/stores/touchPoint-store";
import Person from "../../src/model/person";

const testTouchPoints = [
    { id: 'touchPoint1', method: 'Phone', discussed: 'tp1 comment', date: '2019-01-01Z05:00:00', initiatedByMe: false, person: 'testPerson1' },
    { id: 'touchPoint2', method: 'Meeting', discussed: 'tp2 comment', date: '2019-01-02Z05:00:00', initiatedByMe: true, person: 'testPerson1' }
]
const testPeople = [
    { id: 'testPerson1', firstName: 'Test', lastName: 'Person 1' },
    { id: 'testPerson2', firstName: 'Test', lastName: 'Person 2' },
]

const testGroups = [
    { id: '1234', name: 'test 1' },
    { id: '5678', name: 'test 2' }
]

function serializedState() {
    return {
        peopleStore: { people: testPeople },
        touchPointStore: { touchPoints: testTouchPoints },
        personGroupStore: { personGroups: testGroups }
    }
}

test('expect RootStore to load', () => {
    const root = new RootStore({})
    expect(root.personGroupStore).toBeDefined()
    expect(root.uiStore).toBeDefined()
    expect(root.peopleStore).toBeDefined()
    expect(root.roleStore).toBeDefined()
    expect(root.companyStore).toBeDefined()
    expect(root.commonStore).toBeDefined()
    expect(root.touchPointStore).toBeDefined()
    expect(root.actionItemStore).toBeDefined()
})

test('expect initializeStore to create RootStore object', () => {
    const { rootStore } = initializeStore({})
    expect(rootStore.personGroupStore).toBeDefined()
    expect(rootStore.uiStore).toBeDefined()
    expect(rootStore.peopleStore).toBeDefined()
    expect(rootStore.roleStore).toBeDefined()
    expect(rootStore.companyStore).toBeDefined()
    expect(rootStore.commonStore).toBeDefined()
    expect(rootStore.touchPointStore).toBeDefined()
    expect(rootStore.actionItemStore).toBeDefined()
    expect(rootStore).toBeInstanceOf(RootStore)
})

test('should serialize', () => {
    const { rootStore } = initializeStore({})
    expect(rootStore.toJSON()).toEqual({
        personGroupStore: { personGroups: [] },
        peopleStore: { people: [] },
        roleStore: { roles: [] },
        companyStore: { companies: [] },
        commonStore: { token: '', userName: '' },
        touchPointStore: { touchPoints: [] },
        uiStore: {
            sidebarOpen: false,
            newPersonOpen: false,
            newTouchPointOpen: false,
            newRoleOpen: false,
            newActionItemOpen: false,
            selectedPerson: null,
            selectedCompany: null
        },
        actionItemStore: { actionItems: [] }
    })
})

test('should rehydrate from state', () => {
    jest.resetModules();

    const storeModule = require('../../src/stores/stores')
    const tpModule = require('../../src/stores/touchPoint-store')
    const personModule = require('../../src/model/person')
    const personGroupModule = require('../../src/stores/person-group-store')

    const { rootStore } = storeModule.initializeStore(serializedState())

    expect(rootStore.personGroupStore).toBeDefined()
    expect(rootStore.uiStore).toBeDefined()
    expect(rootStore.peopleStore).toBeDefined()
    expect(rootStore.roleStore).toBeDefined()
    expect(rootStore.companyStore).toBeDefined()
    expect(rootStore.commonStore).toBeDefined()
    expect(rootStore.touchPointStore).toBeDefined()
    expect(rootStore).toBeInstanceOf(storeModule.default)

    // console.log(rootStore.peopleStore.people)
    expect(rootStore.touchPointStore).toBeInstanceOf(tpModule.default)
    expect(rootStore.touchPointStore.touchPoints.length).toEqual(2)
    expect(rootStore.touchPointStore.touchPoints[0].person).toBeInstanceOf(personModule.default)

    expect(rootStore.peopleStore.people[0].id).toEqual('testPerson1')

    expect(rootStore.personGroupStore.personGroups[0].id).toEqual('1234');
})