import RootStore, { initializeStore } from "../../src/stores/stores";
import CompanyStore from "../../src/stores/company-store";
import Company from '../../src/model/company';
import * as companyAPI from '../../src/api/companies';

const testCompaniesFromAPI = [
    { _id: 'company1', Name: 'Company 1', comments: 'Comment 1', Website: 'test1.org' },
    { _id: 'company2', Name: 'Company 2', comments: 'Comment 2', Website: 'test2.org' },
]

const testCompaniesFromState = [
    { id: 'company1State', name: 'Company 1', comments: 'Comment 1', website: 'test1.org' },
    { id: 'company2State', name: 'Company 2', comments: 'Comment 2', website: 'test2.org' },
]

const newCompanyFromAPI = [
    { id: 'newCompanyAPI', name: 'Company 3', comments: 'Comment 3', website: 'test3.org' }
]

test('should load with rootStore', () => {
    const { rootStore } = initializeStore({});
    expect(rootStore.companyStore).toBeInstanceOf(CompanyStore)
    expect(rootStore.companyStore.rootStore).toBe(rootStore)
})

test('should serialize properly', () => {
    const { rootStore } = initializeStore({})
    const store = rootStore.companyStore;

    expect(store.toJSON()).toEqual({ companies: [] })
})

test('should serialize array properly', () => {
    const { rootStore } = initializeStore({})
    const store = rootStore.companyStore;

    store.addCompany(new Company(testCompaniesFromState[0].id, testCompaniesFromState[0].name, testCompaniesFromState[0].website, testCompaniesFromState[0].comments))
    expect(store.toJSON()).toEqual({
        companies: [
            { id: 'company1State', name: 'Company 1', comments: 'Comment 1', website: 'test1.org', personRoles: [] }
        ]
    })
})

test('should sort companies', () => {
    const store = new CompanyStore({
        companies: [
            testCompaniesFromState[1], testCompaniesFromState[0]
        ]
    })

    expect(store.companies.length).toEqual(2)
    expect(store.companies[0].id).toEqual('company2State')
    expect(store.sortedCompanies[0].id).toEqual('company1State')
})

describe('loading functions', () => {
    test('should load from API', async () => {
        companyAPI.getCompanyList = jest.fn(() => {
            return Promise.resolve(testCompaniesFromState)
        })

        const rootStore = new RootStore({});
        const store = rootStore.companyStore
        expect(store.companies.length).toEqual(0)

        await store.loadCompanies()
        expect(store.companies.length).toEqual(2)
    })

    test('should load from API select company', async () => {
        companyAPI.getCompany = jest.fn(() => {
            return Promise.resolve(testCompaniesFromState[0])
        })

        const rootStore = new RootStore({});
        const store = rootStore.companyStore
        expect(store.companies.length).toEqual(0)

        await store.loadCompany('company1State')
        expect(store.companies.length).toEqual(1)
        expect(store.find('company1State')).toBeInstanceOf(Company)
    })

    test('should load from intializedState', async () => {
        const store = new CompanyStore({ companies: testCompaniesFromState });
        expect(store.companies.length).toEqual(2)
    })
})

describe('crud functions', () => {
    test('should add new', async () => {
        companyAPI.addNewCompany = jest.fn(() => {
            return Promise.resolve(newCompanyFromAPI[0])
        })
        const root = new RootStore({ commonStore: { token: 'testToken' } })
        // root.commonStore.setToken('testToken')

        const store = root.companyStore
        let newCompany = new Company(
            'thisShouldNotPersist',
            newCompanyFromAPI[0].name,
            newCompanyFromAPI[0].website,
            newCompanyFromAPI[0].comments
        )
        expect(store.companies.length).toEqual(0)
        //TODO: Make this work
        await store.saveNew(newCompany)
        expect(companyAPI.addNewCompany).toBeCalledWith(newCompany, expect.stringMatching('testToken'))
        expect(store.companies.length).toEqual(1)
        expect(store.companies.find((company) => company.id === 'newCompanyAPI')).toBeInstanceOf(Company)
    })
})