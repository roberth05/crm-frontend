import { initializeStore } from "../../src/stores/stores";
import CommonStore from "../../src/stores/common-store";

test('should load with a rootStore', () => {
    const { rootStore } = initializeStore({});
    const store = rootStore.commonStore;

    expect(store.rootStore).toBe(rootStore)
})

test('should serialize', () => {
    const store = new CommonStore()
    expect(store.toJSON()).toEqual({
        token: '',
        userName: ''
    })
})