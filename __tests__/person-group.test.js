/* eslint-env jest */


import PersonGroup from '../src/model/person-group';

test('should return PersonGroup', () => {
    var group = new PersonGroup()
    group.id = '1234';
    group.name = 'test Group'
    group.contactFrequency = 7

    expect(group.id).toEqual('1234');
});

test('should serialize', () => {
    var group = new PersonGroup()
    group.id = '1234';
    group.name = 'test Group'
    group.contactFrequency = 7

    expect(group.toJSON()).toEqual({
        id: '1234', name: 'test Group', contactFrequency: 7
    });
})

test('should load fromJS', () => {
    const group = PersonGroup.fromJS({
        id: '1234',
        name: 'Test Group 1',
        contactFrequency: 7
    })

    expect(group.id).toEqual('1234')
    expect(group.name).toEqual('Test Group 1')
    expect(group.contactFrequency).toEqual(7)
    expect(group).toBeInstanceOf(PersonGroup)
})

test('should sort based on frequency - shorter frequencies first', () => {
    const group1 = new PersonGroup('group1', 'Test Group 1', 14)
    const group2 = new PersonGroup('group2', 'Test Group 2', 7)

    expect(group1.compare(group2)).toEqual(1)
})

test('should sort based on name', () => {
    const group1 = new PersonGroup('group1', 'Test Group 1', 7)
    const group2 = new PersonGroup('group2', 'Test Group 2', 7)

    expect(group1.compare(group2)).toEqual(-1)
})