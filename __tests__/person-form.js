/* eslint-env jest */

import { shallow, mount } from 'enzyme'
import React from 'react'
import renderer from 'react-test-renderer'

import PersonForm from '../components/person-form.js'
import PersonGroup from '../src/model/person-group';
import Person from '../src/model/person';

import _ from 'lodash';

const mockSave = (data) => {
    console.log(data);
}

const groups = [
    new PersonGroup('1234', 'Test'),
    new PersonGroup('5678', 'Test 2')
]

const person = new Person({ FirstName: 'Test', LastName: 'Tester', _id: 'person1', Birthday: '01-01-1982' })

describe('Form tests', () => {
    it('Loads Groups', () => {
        expect(groups[0].id).toEqual('1234');
        expect(groups[1].id).toEqual('5678');
    })

    it('Renders header', () => {
        const app = mount(<PersonForm onSave={mockSave} groups={groups} />)

        expect(app.find('h3').length).toBe(1);
    })

    it('Renders single select for persongroups', () => {
        const app = mount(<PersonForm onSave={mockSave} groups={groups} />)

        expect(app.find({ id: 'form-select-persongroups', className: 'search' }).length).toBe(1);
    })

    it('should fire handleOnGroupChange', () => {
        const onSaveMock = jest.fn();

        const component = mount(<PersonForm onSave={onSaveMock} groups={groups} person={person} />);
        expect(component.state('person').groups.length).toEqual(0);
        expect(component.state('person')).toBeInstanceOf(Person);
        const instance = component.instance();
        jest.spyOn(instance, 'handleGroupChange');
        instance.handleGroupChange(null, { name: 'persongroups', value: '1234' });
        expect(instance.handleGroupChange).toHaveBeenCalledTimes(1);
        expect(component.state('person').groups.length).toEqual(1);
        expect(component.state('person').groups[0]).toBeInstanceOf(PersonGroup);
    })
})

describe('With Snapshot Testing', () => {
    it('App shows "Hello world!"', () => {
        const component = renderer.create(<PersonForm onSave={mockSave} />)
        const tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})